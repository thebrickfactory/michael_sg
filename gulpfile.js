var gulp = require('gulp');
var del = require('del');
var config = require('./config.json');
var docroot = config.docroot;

// See package.json for dependencies
// Access plugins like this: $.autoprefixer()
var $ = require('gulp-load-plugins')({camelize: true});

/**
 * Error handling
 */

var onError = function(err) {
  $.notify.onError({
    title:    "Gulp",
    subtitle: "Failure!",
    message:  "Error: <%= error.message %>",
    sound:    "Beep"
  })(err);
  this.emit('end');
};


/**
 * Sass compiling
 *
 * - Automatic task
 */

gulp.task('css', function() {
  // del(['../processed/css'], { 'force': true });
  var sass_dest = docroot + config.sass.dest;

  gulp.src(docroot + config.sass.src)
    .pipe($.plumber({errorHandler: onError}))
    .pipe($.sass({
      outputStyle: 'compressed', // compressed, expanded
      errLogToConsole: true,
      sourceComments: true,
      includePaths: require('node-neat').includePaths
    }))
    .pipe($.autoprefixer('ie > 8'))
    .pipe($.size({showFiles: true}))
    .pipe(gulp.dest(sass_dest))
    .pipe($.notify({
      onLast: true,
      message: function () {
        return 'Sass compiled :)';
      }
    }));
});


/**
 * Minify/Rename Javascript
 *
 * - Automatic task
 */

gulp.task('js', function() {

  // Header JS
  gulp.src([

    // Uncomment the scripts you want to include in the header JS file.
    docroot + config.scripts.src_header.jquery,
    docroot + config.scripts.src_header.drupal

  ])
    .pipe($.plumber({errorHandler: onError}))
    // .pipe($.uglify())
    .pipe($.concat('app_header.min.js'))
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest(docroot + config.scripts.dest));

  // Footer JS
  gulp.src([

    // Uncomment the scripts you want to include in the footer JS file.
    docroot + config.scripts.src_footer.menu,
    docroot + config.scripts.src_footer.forms,
    //docroot + config.scripts.src_footer.tabs,
    //docroot + config.scripts.src_footer.equal,
    //docroot + config.scripts.src_footer.modal,
    //docroot + config.scripts.src_footer.tips,
    docroot + config.scripts.src_footer.chosen,
    docroot + config.scripts.src_footer.app

  ])
    .pipe($.plumber({errorHandler: onError}))
    // .pipe($.uglify())
    .pipe($.concat('app_footer.min.js'))
    .pipe($.size({showFiles: true}))
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest(docroot + config.scripts.dest))
    .pipe($.notify({
      onLast: true,
      message: function () {
        return 'JS minified :)';
      }
    }));

});


/**
 * Clears out the generated css/js files.
 */
gulp.task('clear', function() {
  del([docroot + config.sass.dest], { 'force': true });
  del([docroot + config.scripts.dest], { 'force': true });
  del([docroot + config.sprites.dest], { 'force': true });
});


/**
 * Generate sprites
 *
 * - Manual task
 */

gulp.task('sprites', function() {
  var spriteData =
    gulp.src(config.sprites.src) // source path of the sprite images
      .pipe($.spritesmith({
        imgName: 'sprite.png',
        cssName: '_sprite.scss',
        algorithm: 'binary-tree',
        imgPath: docroot + config.sprites.stylesrc, // path needed for imgs in CSS
        padding: 10,
        cssVarMap: function(sprites) {
          sprites.name = 's-' + sprites.name;
        }
      }));

  spriteData.img
    .pipe($.imagemin())
    .pipe(gulp.dest(docroot + config.sprites.dest)); // output path for the sprite
  spriteData.css
    .pipe(gulp.dest(docroot + config.sprites.scssdest)); // output path for the CSS
});

/**
 * Hologram task that runs the CSS task first, and then generates the styleguide using hologram
 */
gulp.task('styleguide', ['css'], function() {
  gulp.src(docroot + 'styleguide/hologram_config.yml')
    .pipe($.hologram({logging:true}));
});

/**
 * Watch task
 *
 * - Watches for changes to JS or SCSS files
 */

gulp.task('watch', function() {
  gulp.watch(docroot + config.sass.src, ['styleguide']); // ,
  gulp.watch(docroot + config.scripts.src, ['js']);
});


/**
 * Default gulp tasks
 *
 * - Runs defined tasks by simply typing 'gulp'
 */

gulp.task('default', ['styleguide', 'js', 'watch']);
