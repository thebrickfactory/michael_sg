api = 2
core = 7.x

projects[] = drupal
projects[] = views_bulk_operations
projects[] = admin_views
projects[] = apachesolr
projects[] = backup_migrate
projects[] = ctools
projects[] = context
projects[] = context_error
projects[] = custom_search
projects[] = date
projects[] = devel
projects[devel_image_provider][version] = "1.x-dev"
projects[] = ds
projects[] = eck
projects[] = entity
projects[] = entityreference
projects[] = features
projects[field_collection][version] = "1.0-beta10"
projects[] = field_group
projects[file_entity][version] = "2.0-beta2"
projects[] = inline_entity_form
projects[] = image_style_quality
projects[] = imce
projects[] = imce_crop
projects[] = imce_wysiwyg
projects[] = libraries
projects[] = link
projects[] = media
projects[] = metatag
projects[] = node_export
projects[] = nodequeue
projects[] = pathauto
projects[] = revisioning
projects[] = smart_trim
projects[] = special_menu_items
projects[] = strongarm
projects[] = styleguide
projects[uuid][version] = "1.0-beta1"
projects[] = token
projects[] = taxonomy_revision
projects[] = views
projects[] = webform
projects[wysiwyg][version] = "2.x-dev"
projects[] = xmlsitemap

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
projects[tbf_default][type] = "module"
projects[tbf_default][download][type] = "git"
projects[tbf_default][download][url] = "git@bitbucket.org:thebrickfactory/tbf_default.git"
projects[tbf_default][download][branch] = develop
projects[tbf_default][subdir] = "tbf"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
projects[tbf_ccblocks][type] = "module"
projects[tbf_ccblocks][download][type] = "git"
projects[tbf_ccblocks][download][url] = "git@bitbucket.org:thebrickfactory/content-config-blocks.git"
projects[tbf_ccblocks][download][branch] = develop
projects[tbf_ccblocks][subdir] = "tbf"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
projects[tbf_data_modules][type] = "module"
projects[tbf_data_modules][download][type] = "git"
projects[tbf_data_modules][download][url] = "git@bitbucket.org:thebrickfactory/tbf_data_modules.git"
projects[tbf_data_modules][download][branch] = master
projects[tbf_data_modules][subdir] = "tbf"

; Themes
projects[ember][subdir] = "ember"
projects[ember][version] = "2.0-alpha4"

projects[mothership][subdir] = "mothership"
projects[mothership][version] = "2.10"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
projects[brickthemesass][type] = "theme"
projects[brickthemesass][download][type] = "git"
projects[brickthemesass][download][url] = "git@bitbucket.org:thebrickfactory/styleguide.git"
projects[brickthemesass][download][branch] = themes/drupal
projects[brickthemesass][subdir] = "tbf"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[ckeditor][type] = "library"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.4/ckeditor_4.5.4_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"