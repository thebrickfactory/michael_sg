
// Open modal in AJAX callback
(function($) {

  /** SORT OPTIONS LIST **/
  $.fn.sortOptions = function() {
    var options = $('option', this);
    var arr = options.map(function(_, o) {
      return {
        t: $(o).text(),
        v: o.value
      };
    }).get();
    arr.sort(function(o1, o2) {
      var t1 = o1.t.toUpperCase(), t2 = o2.t.toUpperCase();

      return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
    });
    options.each(function(i, o) {
      //console.log(i);
      o.value = arr[i].v;
      $(o).text(arr[i].t);
    });
  };

  /** Update main node form. **/
  $(window).bind("load", function() {
    // Change column headers.
    $('.ief-entity-table > thead > tr > th:eq(3)').text('type');

    // Sort drop downs for widgets.
    if ($('#edit-field-abovesidebar-widgets-und-actions-bundle').length > 0) {
      $('#edit-field-abovesidebar-widgets-und-actions-bundle').sortOptions();
      $('#edit-field-leftofsidebar-widgets-und-actions-bundle').sortOptions();
      $('#edit-field-main-widgets-und-actions-bundle').sortOptions();
    }

    // Add preview link to single source checkbox description.
    if ($('#edit-field-enable-single-source').length > 0) {
      var page_link = $('#tab-bar > ul > li:first-child > a').attr('href') + '?preview';
      var description = $('#edit-field-enable-single-source .description').html();
      $('#edit-field-enable-single-source .description').html(description + ' <a href="' + page_link + '" target="_blank">Preview single source page.</a>');
    }

  });

}($j));
