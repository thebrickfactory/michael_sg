TBF: Data Modules

This module implements widget functionality into the page content type. When
you add a "widget" field, it allows to add and re-order widgets in that field.
This field is technically an entityreference field. The widgets are just the
bundles on the "widget" content type.


-------------
CONFIG
-------------
First take a look at the tbf_data_modules.config.inc config file and update
the settings, if you need to.

We need to overwrite some files in the ECK module to get the dropdown/checkbox
functionality integrate. Copy the files from the eck_overrides folder to
(except the .drush_lock file):
sites/all/eck/plugins/property_behavior

Copy the .drush_lock file to sites/all/eck to prevent the module from being
updated.


-------------
WIDGETS
-------------
To add a new widget, you simply add a new bundle to the widget entity type.

The template field on each widget entity is pulled from the template folder
that has the same machine name as the bundle. Make sure to have a default
template file. In each each widget folder, there should be a file for each template in this
format: [widget_machine_name]-[template_name].tpl.php

The [template_name] is what will display in the admin.

To create a new template for a widget, simply create the new file and clear
drupal cache.

When adding new fields to a widget bundle, clear the drupal cache and variables
that match the field name should be available in those templates automatically.



-------------
THEME
-------------
If you are using bricktheme, you only need to add this to your layout file
to output the widgets:
<?php print render($page['display_field_widgets']); ?>

Replace "field_widgets" with the name of the widget fields.