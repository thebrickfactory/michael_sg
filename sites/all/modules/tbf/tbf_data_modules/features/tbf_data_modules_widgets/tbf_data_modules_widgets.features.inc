<?php
/**
 * @file
 * tbf_data_modules_widgets.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function tbf_data_modules_widgets_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function tbf_data_modules_widgets_eck_bundle_info() {
  $items = array(
    'widgets_example_widget' => array(
      'machine_name' => 'widgets_example_widget',
      'entity_type' => 'widgets',
      'name' => 'example_widget',
      'label' => 'Example Widget',
    ),
    'widgets_mikes_test_bundle' => array(
      'machine_name' => 'widgets_mikes_test_bundle',
      'entity_type' => 'widgets',
      'name' => 'mikes_test_bundle',
      'label' => 'Mikes Test Bundle',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function tbf_data_modules_widgets_eck_entity_type_info() {
  $items = array(
    'widgets' => array(
      'name' => 'widgets',
      'label' => 'Widgets',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'widget_reuse' => array(
          'label' => 'Allow this widget to be reused.',
          'type' => 'positive_integer',
          'behavior' => 'checkbox',
        ),
        'wrapper_id' => array(
          'label' => 'Wrapper ID',
          'type' => 'text',
          'behavior' => 'textfield',
        ),
        'classes' => array(
          'label' => 'Classes',
          'type' => 'text',
          'behavior' => 'textfield',
        ),
        'template' => array(
          'label' => 'Template',
          'type' => 'text',
          'behavior' => 'template',
        ),
      ),
    ),
  );
  return $items;
}
