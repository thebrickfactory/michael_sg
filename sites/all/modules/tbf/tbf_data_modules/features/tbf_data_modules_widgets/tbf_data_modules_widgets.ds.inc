<?php
/**
 * @file
 * tbf_data_modules_widgets.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function tbf_data_modules_widgets_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'widgets|mikes_test_bundle|default';
  $ds_layout->entity_type = 'widgets';
  $ds_layout->bundle = 'mikes_test_bundle';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_quote',
        1 => 'field_author',
      ),
    ),
    'fields' => array(
      'field_quote' => 'ds_content',
      'field_author' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['widgets|mikes_test_bundle|default'] = $ds_layout;

  return $export;
}
