<?php
/**
 * @file
 * The checkbox behavior makes a property behave like a checkbox.
 */

//  'entity_save' => 'eck_checkbox_property_entity_save',

$plugin = array(
  'label' => "Checkbox",
  'entity_view' => 'eck_checkbox_property_entity_view',
  'entity_info' => 'eck_checkbox_property_info',
  'default_widget' => 'eck_checkbox_property_widget',
);

function eck_checkbox_property_entity_view($property, $vars) {}

/**
 * How to input a checkbox.
 */
function eck_checkbox_property_widget($property, $vars) {
  $entity = $vars['entity'];
  $checked_status = _eck_checkbox_property_extract_value($entity, $property);
  return array(
    '#type' => 'checkbox',
    '#title' => $vars['properties'][$property]['label'],
    '#default_value' => $checked_status,
  );
}

function eck_checkbox_property_info($property, $vars) {
  $vars['properties'][$property]['type'] = 'integer';
  $vars['properties'][$property]['description'] = t("On/Off checkbox");
  return $vars;
}

/**
 * Helper function that gets the checkbox from an entity.
 * 
 * @param object $entity
 *   an entity object.
 * @param string $property
 *   the name of the property that contains the checkbox.
 * 
 * @return integer
 *   The checkbox of the entity.
 */
function _eck_checkbox_property_extract_value($entity, $property) {
  $checked_status = 0;
  if (isset($entity->{$property})) {
    $checked_status = $entity->{$property};
  }

  return $checked_status;
}
