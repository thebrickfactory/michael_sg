<?php
/**
 * mikes_test_bundle-default.tpl.php
 *
 * VARIABLES
 * - $custom_id: Custom id for the wrapping element.
 * - $custom_classes: Custom classes for the wrapping element.
 * - $field_example_headline
 * - $field_example_closing_text
 * - $field_example_image
 */
?>
<section class="o-band wf">
  <div class="o-ui-row <?php print $custom_classes; ?>" id="<?php print $custom_id; ?>">

  <!-- Quote -->
  <?php if ($field_quote): ?>
    <blockquote><?php print $field_quote; ?>
  <?php endif;?>

    <!-- Cite -->
    <?php if ($field_author): ?>
      <cite><?php print $field_author; ?></cite>
    <?php endif;?>
  </blockquote>

  </div>
</section>