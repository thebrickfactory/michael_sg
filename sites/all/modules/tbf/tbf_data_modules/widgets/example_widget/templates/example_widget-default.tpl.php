<?php
/**
 * example_widget-default.tpl.php
 *
 * VARIABLES
 * - $custom_id: Custom id for the wrapping element.
 * - $custom_classes: Custom classes for the wrapping element.
 * - $field_example_headline
 * - $field_example_closing_text
 * - $field_example_image
 */
?>

<section class="<?php print $custom_classes; ?>" id="<?php print $custom_id; ?>">

  <!-- Headline -->
  <?php if ($field_example_headline): ?>
    <h2 class="has-sub"><?php print $field_example_headline; ?></h2>
  <?php endif;?>

  <!-- Image Field -->
  <?php if ($field_example_image): ?>
    <p>URL To Image: <?php print $field_example_image; ?></p>
  <?php endif;?>

  <!-- Multiple text fields -->
  <?php if ($field_example_multiple_text): ?>
    <?php foreach ($field_example_multiple_text as $row): ?>

      <div>
        <?php print $row; ?>
      </div>

    <?php endforeach; ?>
  <?php endif; ?>

</section>