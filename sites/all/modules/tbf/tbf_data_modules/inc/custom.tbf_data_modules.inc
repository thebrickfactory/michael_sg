<?php
/**
 * If you need code to handle custom functionality for certain fields, create
 * a function here and call it from your template file.
 */

/**
 * Example from WGU: Global - Degrees List: Columns
 */
/*
function tbf_data_modules_global_degrees_list_columns($widget_entity) {
  $degree_columns = array();
  foreach ($widget_entity->field_degrees_list_which_deg['und'] as $row) {

    list(,$collection) = each(entity_load('field_collection_item', array($row['value'])));

    // Get the results from the view.
    $degrees = dm_widget_global_degrees_list_results($collection);
    if (count($degrees) > 0) {
      $degree_columns[] = $degrees;
    }
  }

  if (count($degree_columns) < 1) {
    $degree_columns = FALSE;
  }

  return $degree_columns;
}
*/


/**
 * Returns either an image or a background color.
 * @return [array] $results['image'] or $results['color'] 
 */
function tbf_data_modules_background($bg) {
  $results = array();
  $results['image'] = '';
  $results['color'] = '';
  $results['text_color'] = '';
  $results['link_color'] = '';

  $bg_color = isset($bg['field_background_color']) ? $bg['field_background_color'] : FALSE;
  $bg_image = isset($bg['field_background_image']) ? $bg['field_background_image'] : FALSE;

  if ($bg_image) {
    // Sets background image
    $results['image'] = "background-image: url('" . $bg_image . "'); background-size: cover;";
    // Sets light text color
    $results['text_color'] = 't-white';
  }
  else if ($bg_color) {
    // Sets light text color for dark backgrounds
    if($bg_color == 't-bg-primary' || $bg_color == 't-bg-secondary') {
      $results['text_color'] = 't-white';
      $results['link_color'] = 'c-teaser--white';
    }
    // Sets background color
    $results['color'] = $bg_color;
    // $results['link_color'] = '';
  }

  return $results;
}

function tbf_data_modules_postprocess_publication_grid($entity, $host_nid) {

  drupal_add_js(drupal_get_path('module', 'tbf_data_modules') . '/js/publication_grid.js');

  $ret = array();

  $grid_id = 'grid-widget-' . drupal_html_class($entity->title) . '-' . $host_nid . '-' . $entity->id;
  $ret['grid_ajax_class'] = 'grid-widget ' . $grid_id;

  if(!isset($_GET['grid_ajax_path'])) {
    drupal_add_js(array($grid_id => array('path' => current_path())), 'setting');
    $viewpath = current_path();
    $viewpath = drupal_lookup_path('alias', $viewpath);
  }
  else {
    $viewpath = $_GET['grid_ajax_path'];
  }

  $ret['rows'] = array();

  $args_type = 'all';
  $args_terms = 'all';

  $join = '+';

  // value, tid, target_id
  if(!isset($entity->language)) {
    $entity->language = LANGUAGE_NONE;
  }

  $args_type = "publications";

  if(isset($entity->field_pub_grid_type[$entity->language])) {
    $args_terms = _convert_multiple_field_to_string($entity->field_pub_grid_type[$entity->language], 'tid', $join, TRUE);
  }

  $show_thumbnails = FALSE;
  if(isset($entity->field_pub_grid_show_thumb[$entity->language][0]['value']) && $entity->field_pub_grid_show_thumb[$entity->language][0]['value']) {
    $show_thumbnails = TRUE;
  }

  $view = views_get_view('data_widgets_publication_grid', TRUE);

  $view->preview=TRUE;
  $view->is_cacheable = FALSE;

  $view->set_display('page');

  $view->init_handlers();

  $view->set_arguments(array($args_type,$args_terms));

  $view->override_url = $viewpath;

  $view->pre_execute();

  if(isset($entity->field_pub_grid_display_all[$entity->language][0]['value']) && $entity->field_pub_grid_display_all[$entity->language][0]['value']) {
    $view->set_items_per_page(0);
  }
  else {
    if(isset($entity->field_pub_grid_num_rows[$entity->language][0]['value'])) {
      // Force views pager
      $view->set_items_per_page(intval($entity->field_pub_grid_num_rows[$entity->language][0]['value']) * 3); // 3 column based
    }
  }
  
  $view->set_current_page((isset($_GET['page']) ? $_GET['page'] : 0));

  $view->execute();

//  $exposed_form = $view->display_handler->get_plugin('exposed_form');

//  $ret['exposed_form'] = '<div id="' . $grid_id . '-exposed-form">' . $exposed_form->render_exposed_form(true) . '</div>';

  $form_state = array(
    'view' => $view,
    'display' => $view->display_handler->display,
    'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'), //exposed form plugins are used in Views 3
    'method' => 'get',
    'rerender' => TRUE,
    'no_redirect' => TRUE,
  );

  $form = drupal_build_form('views_exposed_form', $form_state); //create the filter form

  $available_filters = array('pub_type');
  $enabled_filters = array('status');
  if(isset($entity->field_pub_grid_enabled_filters[$entity->language])) {
    foreach($entity->field_pub_grid_enabled_filters[$entity->language] as $filter) {
      // Field mapping
      switch($filter['value']) {
        case 'type':
          $enabled_filters[] = 'pub_type';
          break;
      }
    }
  }
  foreach($form as $key => $filter) {
    if((!in_array($key, $enabled_filters)) && in_array($key, $available_filters)) {
      unset($form[$key]);
    }
  }
  if(isset($form['pub_type'])) {
    $form['pub_type']['#value'] = (isset($_GET['pub_type']) ? $_GET['pub_type'] : 'All');
    if(isset($form['pub_type']['#options']) && count($form['pub_type']['#options']) < 3) {
      unset($form['pub_type']);
    }
  }

  $enabled_sorts = array();
  if(isset($entity->field_pub_grid_enabled_sorts[$entity->language])) {
    foreach($entity->field_pub_grid_enabled_sorts[$entity->language] as $sort) {
      // Field mapping
      switch($sort['value']) {
        case 'title':
          $enabled_sorts[] = 'title ASC';
          $enabled_sorts[] = 'title DESC';
          break;
        case 'date':
          $enabled_sorts[] = 'created ASC';
          $enabled_sorts[] = 'created DESC';
          break;
      }
    }
  }
  foreach($form['sort_bef_combine']['#options'] as $key => $sort) {
    if(!in_array($key, $enabled_sorts)) {
      unset($form['sort_bef_combine']['#options'][$key]);
    }
  }
  if(count($form['sort_bef_combine']['#options']) == 0) {
    unset($form['sort_bef_combine']);
  }
  if(isset($form['sort_bef_combine']) && isset($_GET['sort_bef_combine'])) {
    $form['sort_bef_combine']['#value'] = $_GET['sort_bef_combine'];
  }

  //$form['#action'] = 'nodeurlhere';

  //you now have a form array which can be themed or further altered...
  $he = drupal_render($form);

  $ret['exposed_form'] = '<div id="' . $grid_id . '-exposed-form">' . $he . '</div>';

  $ret['pager'] = $view->query->pager->render(array());

  $view = $view->result;

  $types = node_type_get_types();
  $alter = array(
    'max_length' => 200, //Integer
    'ellipsis' => TRUE, //Boolean
    'word_boundary' => TRUE, //Boolean
    'html' => TRUE, //Boolean
    );

  foreach($view as $view_row) {

    $node = node_load($view_row->nid);

    $row = array();
    $row['nid'] = $node->nid;
    $row['url'] = url('node/' . $node->nid);
    $row['title'] = $node->title;
    $row['type'] = $types[$node->type]->name;

    $row['teaser'] = "";
    if(isset($node->field_pub_summary[$node->language][0]['value'])) {
      $row['teaser'] = views_trim_text($alter, $node->field_pub_summary[$node->language][0]['value']);
    }

    $row['image_url'] = "";
    if($show_thumbnails && isset($node->field_pub_teaser_image[$node->language][0]['uri'])) {
      $row['image_url'] = image_style_url('grid_image', $node->field_pub_teaser_image[$node->language][0]['uri']);
    }

    $row['publication_type'] = "";
    if(isset($node->field_pub_type[$node->language][0]['tid'])) {
      $pubtypes = array();
      foreach($node->field_pub_type[$node->language] as $tid) {
        $term = taxonomy_term_load($tid['tid']);
        $pubtypes[] = $term->name;
      }
      $pubtypes = join(', ', $pubtypes);
      $row['publication_type'] = $pubtypes;
    }

    $row['type_class'] = drupal_html_class($row['publication_type']);

    $ret['rows'][] = $row;

  }

  return $ret;

}

function tbf_data_modules_postprocess_people_grid($entity, $host_nid) {

  //drupal_add_js(drupal_get_path('module', 'tbf_data_modules') . '/js/people_grid.js');

  // Manual override: This is here to avoid modifying the view and have the future option of expose the sort.
  $_GET['sort_bef_combine'] = 'created DESC';

  $ret = array();

  $grid_id = 'grid-widget-' . drupal_html_class($entity->title) . '-' . $host_nid . '-' . $entity->id;
  $ret['grid_ajax_class'] = 'grid-widget ' . $grid_id;

  $ret['rows'] = array();

  $args_terms = 'all';

  $join = '+';

  // value, tid, target_id
  if(!isset($entity->language)) {
    $entity->language = LANGUAGE_NONE;
  }

  if(isset($entity->field_person_grid_person_type[$entity->language])) {
    $args_terms = _convert_multiple_field_to_string($entity->field_person_grid_person_type[$entity->language], 'tid', $join, TRUE);
  }

  $show_thumbnails = FALSE;
  if(isset($entity->field_people_grid_show_image[$entity->language][0]['value']) && $entity->field_people_grid_show_image[$entity->language][0]['value']) {
    $show_thumbnails = TRUE;
  }

  $view = views_get_view('data_widgets_person_grid', TRUE);
  $view->preview=TRUE;
  $view->is_cacheable = FALSE;

  $view->set_display('page');
  $view->init_handlers();

  $view->set_arguments(array($args_terms));

  $view->pre_execute();
/*
  if($show_thumbnails) {
    $view->set_items_per_page(3);
  }
  else {
    $view->set_items_per_page(9);
  }
*/ 

  $view->set_items_per_page(0);

  $view->set_current_page(0);

  $view->execute();

  $view = $view->result;

  $types = node_type_get_types();
  $alter = array(
    'max_length' => 200, //Integer
    'ellipsis' => TRUE, //Boolean
    'word_boundary' => TRUE, //Boolean
    'html' => TRUE, //Boolean
    );

  foreach($view as $view_row) {

    $node = node_load($view_row->nid);

    $row = array();
    $row['nid'] = $node->nid;
    $row['url'] = url('node/' . $node->nid);
    $row['thumbs'] = $show_thumbnails;
    $row['name'] = $node->title;
    $row['title'] = "";
    if(isset($node->field_people_title[$node->language][0]['value'])) {
      $row['title'] = $node->field_people_title[$node->language][0]['value'];
    }

    $row['image_url'] = "";
    if($show_thumbnails && isset($node->field_people_image[$node->language][0]['uri'])) {
      $row['image_url'] = image_style_url('people_grid_image', $node->field_people_image[$node->language][0]['uri']);
    }

    $ret['rows'][] = $row;

  }

  return $ret;

}