<?php

/**
 * Goes through the full database structure bundles and creates the widgets.
 */
function tbf_data_modules_build_widgets() {

  // Define the db bundles to ignore. These ones shouldn't have widgets.
  $db_bundles_ignore = array('popup');

  $db_bundles = field_info_bundles('database_structure');
  $widget_bundles = field_info_bundles('widgets');
  //echo '<pre>'; print_r($db_bundles); exit;

  foreach ($db_bundles as $bundle_name => $bundle) {

    if (in_array($bundle_name, $db_bundles_ignore)) {
      continue;
    }

    if ($bundle_name == 'degrees_data') {
      // In this case we need to create widgets based on field groups.
      tbf_data_modules_bundle_groups_widget($bundle_name, 'Degree: ');

    }
    else {

      // This is a db structure that is not a field group.
      if (!isset($widget_bundles[$bundle_name])) {
        // Create the widget!
        tbf_data_modules_create_widget($bundle['label'], $bundle_name);
      }

    }
  }

  return 'Completed Widget Creation';
}

/**
 * Create a new widget bundle.
 */
function tbf_data_modules_create_widget($label, $bundle_name) {

  // Put together the label.
  if (strpos($label, 'Degree: ') !== false) {
    $label = str_replace('Degree: ', '', $label) . ': Degree Data Group';
  }
  else {
    $label = $label . ': Widget';
  }

  // Create the bundle.
  $new_bundle = new Bundle();
  $new_bundle->name = $bundle_name;
  $new_bundle->label = t($label);
  $new_bundle->entity_type = 'widgets';
  $new_bundle->save();

  drupal_set_message('Created Widget Bundle: ' . $label);

  // If there is not a default template, we need to create one.
  $module_path = drupal_get_path('module', 'tbf_data_modules');
  $template_path = $module_path . '/widgets/' . $bundle_name . '/templates';

  if (!is_dir($template_path)) {
    // Directory doesn't exist. Creat it.
    mkdir($template_path, 0775);

    // Create a default template file with the variable names.
    $vars = _tbf_data_modules_load_vars_only($bundle_name);
    $template_vars = '';
    if (is_array($vars)) {
      foreach ($vars as $var) {
        $template_vars .= "\n" . ' * - $' . $var;
      }
    }

    $template_file_name = $bundle_name . '-default.tpl.php';
    $template_file_path = $template_path . '/' . $template_file_name;

    $html = <<<EOD
<?php
/**
 * {$template_file_name}
 *
 * VARIABLES
 * - \$custom_id: Custom id for the wrapping element.
 * - \$custom_classes: Custom classes for the wrapping element.{$template_vars}
 */
?>

<section class="l-section <?php print \$custom_classes; ?>" id="<?php print \$custom_id; ?>">
</section>
EOD;

    file_put_contents($template_file_path, $html);

    drupal_set_message('Created Default Template (commit this to repo): ' . $label);
  }

}

/**
 * Loop through the fieldgroups in a data structure bundle.
 */
function tbf_data_modules_bundle_groups_widget($bundle_name, $label_suffix='') {
  $widget_bundles = field_info_bundles('widgets');

  // Get all of the fieldgroups
  $groups = field_group_info_groups('database_structure', $bundle_name, 'form', TRUE);

  foreach ($groups as $group_machine_name => $group) {
    if (strpos($group_machine_name, 'group_db') !== false) {
      $bundle_name = str_replace('group_db_', '', $group_machine_name);
      $label = $label_suffix . $group->label;

      // This is a db: structure group. Create the widget.
      if (count($group->children) < 1) {
        continue;
      }

      if (isset($widget_bundles[$bundle_name])) {
        // This bundle already exists.
        continue;
      }

      // TODO: Create template file if it doesn't exist with variables.
      // $group->children: Contains an array of all fields in this group.

      if (!isset($widget_bundles[$bundle_name])) {
        // Create the widget!
        tbf_data_modules_create_widget($label, $bundle_name);
      }
    }
  }
}

/**
 * Returns all the available vars for a database structure.
 */
function _tbf_data_modules_load_vars_only($bundle_name) {
  $groups = field_group_info_groups('database_structure', 'degrees_data', 'form', TRUE);
  $db_bundles = field_info_bundles('database_structure');

  if (isset($db_bundles[$bundle_name])) {
    // Get the fields from the correct db structure.
    $fields = field_read_fields(array('entity_type' => 'database_structure', 'bundle' => $bundle_name));
    $fields = array_keys($fields);
  }
  else {
    $group_name = 'group_db_' . $bundle_name;
    if (!isset($groups[$group_name])) {
      return 'Widget is not connected to a db bundle or field group?';
    }

    // Field Group
    $fieldgroup = $groups[$group_name];
    $fields = $fieldgroup->children;
  }

  return $fields;
}

/**
 * Get friendly name for a bundle.
 */
function tbf_data_modules_friendly_bundle_name($entity_type, $bundle) {
  $info = entity_get_info($entity_type);
  return $info['bundles'][$bundle]['label'];
}

/**
 * Returns a list of templates based on filenames.
 */
function tbf_data_modules_get_all_templates($widget_bundle_name='') {
  $module_dir = dirname(__FILE__);
  $base_dir = $module_dir . '/../widgets';
  if (!empty($widget_bundle_name)) {
    $base_dir .= '/' . $widget_bundle_name;
  }

  if (!is_dir($base_dir)) {
    $templates = array();
    $templates[] = array(
      'name' => 'default',
    );

    return $templates;
  }

  $templates = array();

  $di = new RecursiveDirectoryIterator($base_dir);
  foreach (new RecursiveIteratorIterator($di) as $filename_fullpath => $file) {
    //echo '<p>' . $filename . '</p>';
    if (strpos($filename_fullpath, '.tpl.php') !== false) {
      $cur_filename = $file->getFilename();

      // This is a template file
      // Extract the template name from the file name.
      $file_without_ext = str_replace('.tpl.php', '', $cur_filename);
      $template_name = explode('-', $file_without_ext);

      if (isset($template_name[1])) {
        $templates[] = array(
          'name' => $template_name[1],
          'filename' => $cur_filename,
          'filename_no_ext' => $file_without_ext,
          'widget' => $template_name[0],
        );
      }
    }
  }

  //echo '<pre>'; print_r($templates); exit;
  return $templates;
}