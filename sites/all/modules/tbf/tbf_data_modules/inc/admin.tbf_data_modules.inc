<?php

/**
 * Implements hook_form_alter().
 *
 * Modify the admin form. This is where we clean up the admin forms.
 */
function tbf_data_modules_form_alter(&$form, &$form_state, $form_id) {

  // What should the label for the identifier field be?
  $identifier_title = t('Identifier');

  // Define all possible widget fields.
  $widget_fields = DBWidget_Config::$widget_content_types;

  // Define content types where the widgets are on.
  $content_types = DBWidget_Config::$widget_field_names;

  if(
    (isset($form['#bundle']) && in_array($form['#bundle'], $content_types)) ||
    (isset($form['#entity_type']) && $form['#entity_type'] == 'widgets')
  ) {
    // Add more recent version of jquery
    //drupal_add_js('http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
    //drupal_add_js('var $j = $.noConflict(true);', array('type' => 'inline'));

    // Add module JS/CSS
    $module_path = drupal_get_path('module', 'tbf_data_modules');
    drupal_add_css($module_path . '/css/tbf_data_modules.css');
    drupal_add_js($module_path . '/js/tbf_data_modules.js');
  }

  if (isset($form['#entity_type']) && $form['#entity_type'] == 'widgets') {

    // Hit the Add/Update Entity Form
    if (isset($form['title'])) {
      // Change label and order of the identifier field.
      $form['title']['#weight'] = -99;
      $form['title']['#title'] = $identifier_title;
    }

    //dpm($form);

    // Hide the url redirects
    $form['redirect']['#access'] = FALSE;
  }


  // Update the entity forms.
  foreach ($widget_fields as $widget_field) {

    // Add Entity Form
    if(isset($form[$widget_field]) && isset($form[$widget_field]['und']['form']['title'])) {
      // Change label and order of the identifier field.
      $form[$widget_field]['und']['form']['title']['#weight'] = -99;
      $form[$widget_field]['und']['form']['title']['#title'] = $identifier_title;

      $bundle_name = tbf_data_modules_friendly_bundle_name($form[$widget_field]['und']['form']['#entity_type'], $form[$widget_field]['und']['form']['#entity']->type);
      $form[$widget_field]['und']['form']['#title'] = $bundle_name;

      //dpm($form[$widget_field]['und']['form']);
      break;
    }

    // Update Entity Form
    if (isset($form[$widget_field]['und']['entities'])) {
      //dpm($form[$widget_field]['und']['entities']);

      foreach ($form[$widget_field]['und']['entities'] as &$form_entity) {
        //dpm($form_entity);
        if (isset($form_entity['form']['title'])) {
          $form_entity['form']['title']['#weight'] = -99;
          $form_entity['form']['title']['#title'] = $identifier_title;
          break;
        } // identifier if statement
      }
    }

  }
}
