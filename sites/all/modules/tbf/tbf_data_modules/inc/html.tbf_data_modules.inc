<?php

/**
 * Main router for putting together the html for the widget system.
 *
 * $node: The node entity that is being loaded.
 * $field_name: The field where the widgets are contained. Available options. These
 * should be defined in the config file.
 */
function tbf_data_modules_output($node, $field_name, $process_tokens=TRUE) {
  $lang = $node->language;
  if (!isset($node->{$field_name}[$lang])) {
    return '';
  }

  $html = '';
  foreach ($node->{$field_name}[$lang] as $row) {
    if (!isset($row['target_id'])) {
      continue;
    }

    // Load the widget entity.
    $widget_entity = entity_load_single('widgets', $row['target_id']);

    if (!$widget_entity) {
      continue;
    }

    $vars = _tbf_data_modules_load_vars($node, $widget_entity);
    
    // Get the html for this widget.
    $html .= _tbf_data_modules_render_template($vars, $node, $widget_entity, $process_tokens);
  }

  // If this is true, replace the tokens.
  if ($process_tokens) {
    $html = token_replace($html);
  }

  return $html;
}

/**
 * This function returns an array of field values and which template to use.
 */
function _tbf_data_modules_load_vars($node, $widget_entity) {
  $widget_type = $widget_entity->type;

  // Get the default values.
  $default_values = array(
    'node' => FALSE,
    'url' => url(drupal_get_path_alias(), array('absolute' => TRUE)),
    'widget_entity' => $widget_entity,
    'custom_id' => $widget_entity->wrapper_id,
    'custom_classes' => $widget_entity->classes,
  );

  if(!isset($node->nid)) {
    $node = menu_get_object();
    if ($node) {
      $default_values['node'] = $node;
    }
  }
  else {
    $default_values['node'] = $node;
  }
  
  // Read the fields from the widget.
  $fields = field_read_fields(array('entity_type' => 'widgets', 'bundle' => $widget_type));
  $fields = array_keys($fields);

  // Get the field values.
  $field_values = _tbf_data_modules_field_values($fields, $widget_entity);

  if(function_exists("tbf_data_modules_postprocess_" . $widget_entity->type)) {
    $field_values += call_user_func("tbf_data_modules_postprocess_" . $widget_entity->type, $widget_entity, $node->nid);
  }

  $values = $default_values + $field_values;

  // Grab the template.
  $template = !empty($widget_entity->template) ? $widget_entity->template : 'default';
  
  return array('data' => $values, 'template' => $template);
}

/**
 * This returns the html for the correct template file.
 */
function _tbf_data_modules_render_template($vars, $node, $widget_entity, $process_tokens) {
  $template = 'data_module-' . $widget_entity->type . '-' . $vars['template'];
  $vars['data']['process_tokens'] = $process_tokens;
  return theme($template, $vars['data']);
}