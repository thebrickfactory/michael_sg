<?php

/**
 * Returns an array of all field values.
 */
function _tbf_data_modules_field_values($fields, $entity) {

  $data = array();
  foreach ($fields as $field_name) {

    // If this is the field columns field, handle it separately.
    if ($field_name == 'field_columns') {
      $data[$field_name] = tbf_data_modules_field_columns($entity);
      continue;
    }

    // Is this a single value or multiple values?
    $field_info = field_info_field($field_name);
    if ($field_info['cardinality'] > 1 || $field_info['cardinality'] == -1) {

      // Multiple Values
      $values = array();
      if (isset($entity->{$field_name}[LANGUAGE_NONE][0])) {
        $row_num = 0;
        foreach ($entity->{$field_name}[LANGUAGE_NONE] as $row) {
          $values[] = _tbf_data_modules_field_types($entity, $field_name, $row, $row_num);
          $row_num++;
        }
      }

      $data[$field_name] = $values;

    }
    else {

      // Single Value

      $value = FALSE;
      if (isset($entity->{$field_name}[LANGUAGE_NONE][0])) {
        $value = _tbf_data_modules_field_types($entity, $field_name, $entity->{$field_name}[LANGUAGE_NONE][0]);
      }

      $data[$field_name] = $value;

    }

  }

  if (isset($_GET['debug'])) {
    echo '<pre>'; print_r($data); exit;
  }

  return $data;
}


/**
 * Handles the html output for a field, based on the field type.
 */
function _tbf_data_modules_field_types($entity, $field_name, $field_value, $row_num=0) {

  $entity_type = $entity->entityType();

  $html = '';
  if (isset($field_value['revision_id'])) {

    // Field Collection
    $html = tbf_data_modules_field_collection($field_value);

  }  else if (isset($field_value['value']) && isset($field_value['safe_value'])) {

    // Text Field
    $value = field_view_value($entity_type, $entity, $field_name, $field_value);
    $html = render($value);

  }
  else if (isset($field_value['value'])) {

    // List Field...use the selected machine name instead
    $html = $field_value['value'];

  }
  else if (isset($field_value['fid'])) {

    if (strpos($field_name, 'video') !== false) {
      $file_object = file_load($field_value['fid']);
      $file_view = file_view($file_object);
      $html = drupal_render($file_view);
    }
    else {
      // Image/File Field...return the url for the file/image.
      $html = file_create_url($entity->{$field_name}[LANGUAGE_NONE][0]['uri']);
    }

  }
  else if (isset($field_value['url'])) {

    // Link Field
    if (!empty($field_value['title'])) {
      $html = '<a href="' . $field_value['url'] . '">' . $field_value['title'] . '</a>';
    }
    else {
      $html = $field_value['url'];
    }

  }
  else {
    /*
    echo '<h1>FIELD TYPE NOT DEFINED</h1>';
    echo $field_name;
    //echo '<pre>'; print_r($entity); echo '</pre>';
    exit;
    */
  }

  if (empty($html)) {
    $html = FALSE;
  }

  return $html;
}

/**
 * Handles field collections. Returns an array that has each field in each
 * row.
 */
function tbf_data_modules_field_collection($row) {
  // This is a field collection.
  $field_collection = entity_load('field_collection_item', array($row['value']));
  list(, $collection_entity) = each($field_collection);

  $collection_values = array();
  foreach ($collection_entity as $collection_field_name => $collection_field) {
    if (!is_array($collection_field) || $collection_field_name == 'rdf_mapping') {
      // This is a value that is not a field.
      continue;
    }

    // Get this value for this collection field.
    $value = isset($collection_field[LANGUAGE_NONE][0]) ? $collection_field[LANGUAGE_NONE][0] : '';
    $collection_values[$collection_field_name] = _tbf_data_modules_field_types($collection_entity, $collection_field_name, $value, 0);
  }

  return $collection_values;
}

/**
 * Handle the field_columns field.
 */
function tbf_data_modules_field_columns($entity) {
  if (!isset($entity->field_columns['und'][0])) {
    return FALSE;
  }

  $header = array();
  $body = array();
  foreach ($entity->field_columns['und'] as $row) {
    list(, $collection) = each(entity_load('field_collection_item', array($row['value'])));

    $header[] = isset($collection->field_column_header['und'][0]['value']) ? $collection->field_column_header['und'][0]['value'] : '';
    $body[] = isset($collection->field_column_value['und'][0]['value']) ? $collection->field_column_value['und'][0]['value'] : 'field_column_value';
  }

  return array('header' => $header, 'body' => $body);
}

/**
 * Takes an array and splits the array into $num_of_columns.
 */
function tbf_data_modules_split_array($array, $num_of_columns) {
  $total_rows = count($array);

  // How many results per column?
  $num_per_column = $total_rows / $num_of_columns;
  $num_per_column_int = (int)$num_per_column;
  if ($num_per_column > $num_per_column_int) {
    $num_per_column = $num_per_column_int + 1;
  }
  else {
    $num_per_column = $num_per_column_int;
  }

  return array_chunk($array, $num_per_column, true);
}
