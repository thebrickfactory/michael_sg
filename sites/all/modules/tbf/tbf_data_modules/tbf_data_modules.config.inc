<?php

// We define a class, since you can't assign an array to constants PHP < 5.6
class DBWidget_Config {

  // The name of the content types that have the widget functionality.
  public static $widget_content_types = array('page', 'webform');

  // The name of the fields that have the data modules attached to them.
  public static $widget_field_names = array('field_widgets');

}