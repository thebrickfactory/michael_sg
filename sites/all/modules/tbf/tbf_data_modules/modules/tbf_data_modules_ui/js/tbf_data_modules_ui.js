(function ($, Drupal) {

  var _excludelist = [];
/*
  function data_modules_ui_state(show) {
    if(show) {
      $('.mask').hide();
      //jQuery.unblockUI();
    } else {
      $('.mask').show();
      //jQuery.blockUI({ overlayCSS: { backgroundColor: '#000000' } });
    }
  }
*/

  
  function _scrollToTop() {
    var body = $("html, body");
    body.animate({scrollTop: $("#edit-field-widgets").offset().top - 100}, '500', 'swing', function() {}); // scrolling up
  }

  function hook_close_button() {
    $('.addsection-header .editsection-close a').click(function(e) {
      e.preventDefault();
      $('.form-wrapper input.form-submit[value="Cancel"]').trigger('mousedown');
      $('.form-wrapper input.form-submit[value="No, cancel"]').trigger('mousedown');
      data_modules_mode_idle();
    });
  }

  function sections_selector_visible(mode) {
    if(mode) {
      $('#addsection-wrapper-all').show();
    }
    else {
      $('#addsection-wrapper-all').hide();
    }
  }

  function addsection_button_visible(mode) {
    if(mode) {
      $('#addsection-button-wrapper').show();
    }
    else {
      $('#addsection-button-wrapper').hide();
    }
  }
  
  data_modules_mode_idle = function(ajax, response, status) {
    sections_selector_visible(false);
    addsection_button_visible(true);
    $('.table-type-widgets tr, .tabledrag-toggle-weight-wrapper').show();
    _scrollToTop();
  };

  data_modules_validation_errors = function(ajax, response, status) {
    $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
    $('.ief-form').closest('tr').show();
    $('.ief-form').show();
    sections_selector_visible(false);
    addsection_button_visible(false);
    hook_close_button();
    $('.ief-form').prepend(response.data);
    _scrollToTop();
  };

  data_modules_mode_edit = function(ajax, response, status) {
    sections_selector_visible(false);
    addsection_button_visible(false);
    $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
    $('.ief-form').closest('tr').show();
    $('.ief-form').show();
    hook_close_button();
    _scrollToTop();
  };

  data_modules_mode_delete = function(ajax, response, status) {
    sections_selector_visible(false);
    addsection_button_visible(false);
    $('#edit-field-widgets tr.ief-row-form td > .delete-section-wrapper').clone(true,true).appendTo('#edit-field-widgets tr.ief-row-entity-form td.inline-entity-form-widgets-table_formatted_entity + td').parent().addClass("delete-section-td");
    $('#edit-field-widgets tr.ief-row-form').remove();
    $('#edit-field-widgets tr.ief-row-entity-form td.inline-entity-form-widgets-table_formatted_entity').parent().addClass('row-delete-wrapper');
    $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
    $('.ief-form').closest('tr').show();
    $('.ief-form').show();
    _scrollToTop();
  };

  data_modules_action_delete = function(ajax, response, status) {
    sections_selector_visible(false);
    addsection_button_visible(true);
  };

  data_modules_mode_newsection = function(ajax, response, status) {
    $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
    $('.ief-form').closest('tr').show();
    $('.ief-form').show();
    sections_selector_visible(false);
    addsection_button_visible(false);
    hook_close_button();
  };

  data_modules_mode_existingsection = function(ajax, response, status) {
    $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
    $('#edit-field-widgets > div').hide();
    _scrollToTop();
  };

  data_modules_action_save = function(ajax, response, status) {
    $('.table-type-widgets tr, .tabledrag-toggle-weight-wrapper').show();
    sections_selector_visible(false);
    addsection_button_visible(true);
    _scrollToTop();
  };
  
  data_modules_action_cancel = function(ajax, response, status) {
    $('.table-type-widgets tr, .tabledrag-toggle-weight-wrapper').show();
    sections_selector_visible(false);
    addsection_button_visible(true);
    _scrollToTop();
  };

  data_modules_reference_action_cancel = function(ajax, response, status) {
    $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').show();
    _scrollToTop();
  };

  Drupal.ajax.prototype.commands.data_modules_mode_idle = data_modules_mode_idle;
  Drupal.ajax.prototype.commands.data_modules_validation_errors = data_modules_validation_errors;
  Drupal.ajax.prototype.commands.data_modules_mode_edit = data_modules_mode_edit;
  Drupal.ajax.prototype.commands.data_modules_mode_delete = data_modules_mode_delete;
  Drupal.ajax.prototype.commands.data_modules_action_delete = data_modules_action_delete;
  Drupal.ajax.prototype.commands.data_modules_mode_newsection = data_modules_mode_newsection;
  Drupal.ajax.prototype.commands.data_modules_mode_existingsection = data_modules_mode_existingsection;
  Drupal.ajax.prototype.commands.data_modules_action_save = data_modules_action_save;
  Drupal.ajax.prototype.commands.data_modules_action_cancel = data_modules_action_cancel;
  Drupal.ajax.prototype.commands.data_modules_reference_action_cancel = data_modules_reference_action_cancel;

  Drupal.behaviors.data_modules = {
    attach: function(context, settings) {

      // Existing entities wont be saved
      _excludelist = settings.tbf_data_modules_ui.exclude;

      // Initial form setup
      $('form.node-form').once('ui_setup', function() {

        $(this).append('<div class="mask" style="display: none;"><div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div></div>');

        $('#addsection-wrapper').prepend('<div class="sections-close close-button"><a href="#">Close</a></div>');
        $('.sections-close a').click(function(e) {
          e.preventDefault();
          $('.ief-ui-reference-cancel').trigger('mousedown');
          _scrollToTop();
          data_modules_mode_idle();
        });

        $('#addsection-button').click(function() {
          addsection_button_visible(false);
          sections_selector_visible(true);
          $('a[href="#edit-addsectiontabs-tabs-newsection"]').trigger('click');
        });

        var el = ['.form-item-title', '.builder-wrapper'];
        for(var i=0; i < el.length;i++) {
          var o = $(el[i]);
          var d = o.find('.description').html();
          if(d != '') {
            o.find('.description').remove();
            o.append('<span><a class="icon-info" href="#"><span>What\'s this?</span><div class="info-popup">' + d + '</div></a></span>');
          }
        }

        $('a.new-section-link').click(function(e) {
          e.preventDefault();
          $('select.ief-ui-selector').val($(this).data('bundle'));
          $('.inputaddnew').trigger('mousedown');
        });

        // Hook up autofilter
        $("#edit-addsectiontabs-tabs-existingsection").prepend('<input id="sections-search-input" class="search_input" placeholder="Search sections" type="search" />');

        var _type_options = '<option selected value="all">Show all types</option>';
        for(var machine_name in Drupal.settings.tbf_data_modules_ui.bundles) {
          _type_options += '<option value="' + machine_name + '">' + Drupal.settings.tbf_data_modules_ui.bundles[machine_name].human + '</option>';
        }

        $("#edit-addsectiontabs-tabs-existingsection").prepend('<select id="sections-type-filter">' + _type_options + '</select>');
        $("#edit-addsectiontabs-tabs-newsection").prepend('<select id="sections-new-type-filter">' + _type_options + '</select>');

        $('#sections-type-filter').change(function() {
          $('#sections-search-input').val('');
          if($(this).val() == 'all') {
            $('#edit-addsectiontabs-tabs-existingsection ul.data-modules li').show();
          }
          else {
            $('#edit-addsectiontabs-tabs-existingsection ul.data-modules li').hide();
            $('#edit-addsectiontabs-tabs-existingsection ul.data-modules a[data-bundle="' + $(this).val() + '"]').parent().show(); 
          }
        });

        $('#sections-new-type-filter').change(function() {
          $('#sections-search-input').val('');
          if($(this).val() == 'all') {
            $('#edit-addsectiontabs-tabs-newsection ul.data-modules li').show();
          }
          else {
            $('#edit-addsectiontabs-tabs-newsection ul.data-modules li').hide();
            $('#edit-addsectiontabs-tabs-newsection ul.data-modules a[data-bundle="' + $(this).val() + '"]').parent().show(); 
          }
        });
      
      });
      // End initial form setup

      $('a.existing-section-link').click(function(e) {
        e.preventDefault();
        $('#edit-field-widgets input.form-autocomplete').val($(this).data('title') + '(' + $(this).data('id') + ')');
        $('#edit-field-widgets input.ief-entity-submit').trigger('mousedown');
        data_modules_mode_idle();
      });

    }
  }

  $(document).ready(function() {

    $(document).ajaxStart(function() {
      $('.mask').show();
    });
    $(document).ajaxStop(function() {
      $('.mask').hide();
    });

    $('a[href="#edit-addsectiontabs-tabs-newsection"]').click(function() {
      $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
      $('.ief-ui-reference-cancel').trigger('mousedown');
      _scrollToTop();
    });

    $('a[href="#edit-addsectiontabs-tabs-existingsection"]').click(function() {

      $('.table-type-widgets > tbody > tr, .fieldset-wrapper > .tabledrag-toggle-weight-wrapper').hide();
      _scrollToTop();
      $('#sections-search-input').val('');

      $('.inputaddexisting').trigger('mousedown');

      $('#edit-addsectiontabs-tabs-existingsection .fieldset-wrapper').hide();

      $.ajax({
        type: "POST",
        data: {excludelist: _excludelist},
        url: Drupal.settings.basePath + 'data-modules-ui/existing',
        success:function(data) {
          if(data.length) {
            var existingsections_html = '<ul class="data-modules">';
          } else {
            var existingsections_html = '<p>There are no existing sections to choose</p>';
          }
          for(var i=0; i < data.length;i++) {
            existingsections_html += '<li class="data-module-wrapper">';
            existingsections_html += '<a class="existing-section-link" href="#" data-title="' + data[i].title + '" data-id="' + data[i].id + '" data-bundle="' + data[i].bundle + '">';
            existingsections_html += '<div class="section-image-wrapper">';
            existingsections_html += '<img src="' + Drupal.settings.tbf_data_modules_ui.bundles[data[i].bundle].icon + '" />';
            existingsections_html += '</div>';
            existingsections_html += '<div class="section-info-wrapper">';
            existingsections_html += '<h4>' + data[i].title + '</h4>';
            existingsections_html += '<p>' + Drupal.settings.tbf_data_modules_ui.bundles[data[i].bundle].description + '</p>';
            existingsections_html += '</div>';
            existingsections_html += '</a>';
            existingsections_html += '</li>';
          }
          if(data.length) {
            existingsections_html += '</ul>';
          }

          $('#edit-addsectiontabs-tabs-existingsection .fieldset-wrapper').html(existingsections_html);
          $('#edit-addsectiontabs-tabs-existingsection .fieldset-wrapper').show();

          $('#sections-search-input').each(function() {
            var el = $(this);
            var search = $(this).parent().find('ul.data-modules');
            el.fastLiveFilter(search);
          });

          Drupal.attachBehaviors();

        }
      });
    });

  });

})(jQuery, Drupal);