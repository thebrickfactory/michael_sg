<?php
/**
 * The main page callback for managing programming-content blocks.
 */
function tbf_ccblocks_admin() {
  if (isset($_GET['delete'])) {
    tbf_ccblocks_delete($_GET['delete']);
  }

  $description = <<<EOD
<h3>What is a Content-Config Block?</h3>
<p>A content-config block is a block that has default values that can be exported to a feature module, but also can be modified by the client on the live site without having to export the feature module when updated.</p><blockquote><p>When the block is updated in this area, the feature module will need to be regenerated to be exported. When the block is updated through the "edit" link on the front end or in the blocks system, the feature module will not need to be regenerated.</p></blockquote>
EOD;

  // Get the blocks.
  $content_config_blocks = variable_get('content_config_blocks', array());
  $rows = array();
  foreach ($content_config_blocks as $delta => $block) {
    $rows[] = array(
      '<a href="/admin/structure/content-config-block/' . $delta . '">' . $delta . '</a>',
      $block['admin_title'],
      '<a href="?delete=' . $delta . '">Delete</a>'
    );
  }

  $table = theme('table', array(
    'header' => array('Delta', 'Admin Title', ''),
    'rows' => $rows,
    'empty' => 'No Content-Config Blocks',
  ));

  return '<p><a href="/admin/structure/content-config-block/add">Create a new Content-Config Block</a></p>' . $table . $description;
}

/**
 * The form for adding/updating a programming-content block.
 */
function tbf_ccblocks_form($form, &$form_state, $delta='') {
  if ($delta == 'add') {
    $delta = '';
  }

  $admin_title = '';
  $default_title = '';
  $default_content = '';
  $class = '';
  if (!empty($delta)) {
    // Load the details for this block.
    $content_config_blocks = variable_get('content_config_blocks', array());
    if (!isset($content_config_blocks[$delta])) {
      return array('text' => array('#markup' => 'That Content-Config block does not exist.'));
    }

    $admin_title = $content_config_blocks[$delta]['admin_title'];
    $default_title = $content_config_blocks[$delta]['default_title'];
    $default_content = is_array($content_config_blocks[$delta]['default_content']) ? $content_config_blocks[$delta]['default_content']['value'] : $content_config_blocks[$delta]['default_content'];
    $class = $content_config_blocks[$delta]['class'];

    $form['delta'] = array('#type' => 'hidden', '#value' => $delta);
    $form['delta_display'] = array('#markup' => '<strong>Delta:</strong> ' . $delta);
  }
  else {
    $form['new'] = array(
      '#type' => 'machine_name',
      '#title' => t('Delta'),
      '#default_value' => '',
      '#maxlength' => 21,
      '#machine_name' => array(
        'exists' => 'tbf_ccblocks_exists',
      ),
    );
  }

  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Title'),
    '#description' => t('This is what appear on admin screens.'),
    '#default_value' => $admin_title,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['default_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Title'),
    '#description' => t('This is what appears as the default title for the block.'),
    '#default_value' => $default_title,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $form['default_content'] = array(
    '#type' => 'text_format',
    '#title' => t('Default Content'),
    '#description' => t('This is what appears as the default html for the block.'),
    '#default_value' => $default_content,
    '#format' => isset($content_config_blocks[$delta]['default_content']['format']) ? $content_config_blocks[$delta]['default_content']['format'] : NULL,
    '#required' => FALSE,
  );

  $form['class'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS Class'),
    '#description' => t('This is the class that is attached to the wrapper div around the block. This cannot be modified by the client.'),
    '#default_value' => $class,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $form['update'] = array('#type' => 'submit', '#value' => 'Submit');
  return $form;
}

function tbf_ccblocks_exists($delta) {
  // Make sure this machine name is unique.
  $content_config_blocks = variable_get('content_config_blocks', array());

  if (isset($content_config_blocks[$delta])) {
    form_set_error('new', 'That machine name already exists.');
  }
}

/**
 * Saves/Updates the Content-Config block.
 */
function tbf_ccblocks_form_submit(&$form, &$form_state) {
  $form_values = $form_state['values'];
  $delta = isset($form_values['new']) ? $form_values['new'] : $form_values['delta'];

  // We just need to save the element into the array.
  $content_config_blocks = variable_get('content_config_blocks', array());

  $content_config_blocks[$delta] = array(
    'admin_title' => $form_values['admin_title'],
    'default_title' => $form_values['default_title'],
    'default_content' => $form_values['default_content'],
    'class' => $form_values['class'],
  );

  variable_set('content_config_blocks', $content_config_blocks);

  if (isset($form_values['new'])) {
    drupal_set_message('The new Content-Config Block has been created.');
  }
  else {
    drupal_set_message("The Content-Config Block '$delta' has been updated.");
  }

  drupal_goto('admin/structure/content-config-block');
}

/**
 * Deletes a Content-Config block.
 */
function tbf_ccblocks_delete($delta) {
  $content_config_blocks = variable_get('content_config_blocks', array());
  if (isset($content_config_blocks[$delta])) {
    unset($content_config_blocks[$delta]);
    variable_set('content_config_blocks', $content_config_blocks);
  }

  drupal_set_message("The Content-Config Block '$delta' has been deleted.");
  drupal_goto('admin/structure/content-config-block');
}