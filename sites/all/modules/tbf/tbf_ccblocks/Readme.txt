ABOUT
------
The problem with the default block system is that we can’t extract them to code
for easy deployment among servers. The problem with a module like Boxes, which
makes blocks exportable through features, is that when they are updated on live,
the feature module needs to be regenerated...which risks accidentally overriding
boxes that are updated directly on live.

This is why this module was created. You can create a content-config block via
the admin that is considered configuration and exportable via features. On the
frontend, the client can modify the content-config block (content) portion and
their changes will show up on the site...but the config portion will not be
touched. So even if you deploy new content config block changes through features
to live, it will not overwrite any changes the client made to the content
portion of that block.



INSTRUCTIONS
------------
To create/update/delete content-config blocks, go here:
/admin/structure/content-config-block

That manages the config portion of these blocks. If you are working on a site,
this is where you want to apply changes, so you can export the changes via
features.

After creating a new block you will want to clear drupal caches (or you will
see a drupal warning show up).

You can display the content config block like any other drupal block. You can
also print out the results of the block directly like this:
<?php print tbf_ccblocks_get_html('maching_name_of_block'); ?>

If the content portion of a content config block has been updated, and if you
want to revert that content config block to what is set in the configuration...
modify the content-config block in the front end and check the revert option.



FEATURES
--------
This module saves all content config blocks into one drupal variable:
content_config_blocks . Use the Strongarm module to export the value of the
variable in features.
