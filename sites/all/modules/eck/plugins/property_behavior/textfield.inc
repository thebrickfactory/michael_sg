<?php
/**
 * @file
 * The textfield behavior makes a property behave like a textfield.
 */

//  'entity_save' => 'eck_textfield_property_entity_save',

$plugin = array(
  'label' => "Textfield",
  'entity_view' => 'eck_textfield_property_entity_view',
  'entity_info' => 'eck_textfield_property_info',
  'default_widget' => 'eck_textfield_property_widget',
);

function eck_textfield_property_entity_view($property, $vars) {}

/**
 * How to input a textfield.
 */
function eck_textfield_property_widget($property, $vars) {
  $entity = $vars['entity'];
  $text_value = _eck_textfield_property_extract_value($entity, $property);
  return array(
    '#type' => 'textfield',
    '#title' => $vars['properties'][$property]['label'],
    '#default_value' => $text_value,
  );
}

function eck_textfield_property_info($property, $vars) {
  $vars['properties'][$property]['type'] = 'text';
  $vars['properties'][$property]['description'] = t("Single textfield");
  return $vars;
}

/**
 * Helper function that gets the textfield from an entity.
 *
 * @param object $entity
 *   an entity object.
 * @param string $property
 *   the name of the property that contains the textfield.
 *
 * @return text
 *   The textfield of the entity.
 */
function _eck_textfield_property_extract_value($entity, $property) {
  $text_value = "";
  if (isset($entity->{$property})) {
    $text_value = $entity->{$property};
  }

  return $text_value;
}
