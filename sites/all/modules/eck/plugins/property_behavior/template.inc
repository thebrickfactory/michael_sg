<?php
/**
 * @file
 * The textfield behavior makes a property behave like a textfield.
 */

//  'entity_save' => 'eck_template_property_entity_save',

$plugin = array(
  'label' => "Templates",
  'entity_view' => 'eck_template_property_entity_view',
  'entity_info' => 'eck_template_property_info',
  'default_widget' => 'eck_template_property_widget',
);

function eck_template_property_entity_view($property, $vars) {}

/**
 * How to input a textfield.
 */
function eck_template_property_widget($property, $vars) {
  $entity = $vars['entity'];
  $selected_value = _eck_template_property_extract_value($entity, $property);

  // Get the template options for this widget.
  $entity_info = entity_extract_ids('widgets', $entity);
  //echo '<pre>'; print_r($entity_info); exit;

  $templates = tbf_data_modules_get_all_templates($entity_info[2]);
  $template_options = array();
  foreach ($templates as $row) {
    $template_name = $row['name'];
    $friendly_name = ucwords(str_replace('_', ' ', $template_name));
    $template_options[$template_name] = $friendly_name;
  }

  return array(
    '#type' => 'select',
    '#title' => $vars['properties'][$property]['label'],
    '#default_value' => $selected_value,
    '#options' => $template_options,
  );
}

function eck_template_property_info($property, $vars) {
  $vars['properties'][$property]['type'] = 'text';
  $vars['properties'][$property]['description'] = t("Single textfield");
  return $vars;
}

/**
 * Helper function that gets the textfield from an entity.
 *
 * @param object $entity
 *   an entity object.
 * @param string $property
 *   the name of the property that contains the textfield.
 *
 * @return text
 *   The textfield of the entity.
 */
function _eck_template_property_extract_value($entity, $property) {
  $selected_value = "default";
  if (isset($entity->{$property})) {
    $selected_value = $entity->{$property};
  }

  return $selected_value;
}
