<?php
/***
 * Custom Home Page layout.
 **/
?>

<?php
/** PHP variables that are used to manually display boxes and blocks.
 *
 * [Programming Block]
 * $problem_solvers = module_invoke('tbf_grid_thumbs', 'block_view', 'meet_problem_solvers_home');
 * <div class="row lightgray-bg">
 *   <div class="large-12 columns"><h2 class="title text-center"><?php echo isset($problem_solvers['subject']) ? $problem_solvers['subject'] : ''; ?></h2><?php print render($problem_solvers); ?></div>
 * </div>
 *
 * [Box]
 * $under_col1 = tbf_blocks_get_box('home_underslider_1');
 * <?php print render($under_col1); ?>
 *
 * [Webform]
 * $problem_solvers_daily_form = module_invoke('webform', 'block_view', 'client-block-1775');
 * <?php print render($problem_solvers_daily_form['content']); ?>
 *
*/
?>

<?php include 'common/header.inc'; ?>

<?php include 'common/breadcrumbs.inc'; ?>
<?php include 'common/messages.inc'; ?>

<div class="o-box c-featured-pane t-flex-center">
      <blockquote class="c-featured-pane__quote">Frightened I am <cite>Yoda</cite></blockquote>
</div>


<div class="o-band">
  <div class="o-grid">
    <ul class="bg-sm-2 bg-md-4 bg-lg-4">
      <li><div class="c-slide--top-padded wf"><h4 class="c-slide__title">Once upon a time, in a land far away.</h4></div></li>
      <li><div class="c-slide--top-padded wf"><h4 class="c-slide__title">Once upon a time, in a land far away.</h4></div></li>
      <li><div class="c-slide--top-padded wf"><h4 class="c-slide__title">Once upon a time, in a land far away.</h4></div></li>
      <li><div class="c-slide--top-padded wf"><h4 class="c-slide__title">Once upon a time, in a land far away.</h4></div></li>
    </ul>
  </div>
</div>

<div class="o-band wf">
    <div class="o-ui-row t-flex-center">
      <blockquote>Luke... I am your father<cite>Vader</cite></blockquote>
    </div>
</div>

<div class="o-band">
  <div class="o-grid">

    <h2 class="t-text-center">Popular posts</h2>

    <div class="o-ui-row">
          <div class="o-grid c-teaser">
            <div class="lg-4 c-teaser__image">
              <img src="http://placehold.it/300x186">
            </div>
            <div class="lg-8">
              <h3 class="c-teaser__title">
                An Intranet Manager's guide to project success
              </h3>
              <p class="c-teaser__content">
                Mindfulness. If you’re still thinking this is merely a touchy-feely trend practiced by yogis, creatives and the business elite – you’re way behind. Thousands of people in organizations around the world are now benefiting from the improved performance, communication, relationships...
              </p>
              <a href="#" class="c-teaser__link">Read More</a>
            </div>
          </div>
        </div>

    <div class="o-ui-row">
          <div class="o-grid c-teaser">
            <div class="lg-4 c-teaser__image">
              <img src="http://placehold.it/300x186">
            </div>
            <div class="lg-8">
              <h3 class="c-teaser__title">
                An Intranet Manager's guide to project success
              </h3>
              <p class="c-teaser__content">
                Mindfulness. If you’re still thinking this is merely a touchy-feely trend practiced by yogis, creatives and the business elite – you’re way behind. Thousands of people in organizations around the world are now benefiting from the improved performance, communication, relationships...
              </p>
              <a href="#" class="c-teaser__link">Read More</a>
            </div>
          </div>
        </div>

    <div class="o-ui-row">
          <div class="o-grid c-teaser">
            <div class="lg-4 c-teaser__image">
              <img src="http://placehold.it/300x186">
            </div>
            <div class="lg-8">
              <h3 class="c-teaser__title">
                An Intranet Manager's guide to project success
              </h3>
              <p class="c-teaser__content">
                Mindfulness. If you’re still thinking this is merely a touchy-feely trend practiced by yogis, creatives and the business elite – you’re way behind. Thousands of people in organizations around the world are now benefiting from the improved performance, communication, relationships...
              </p>
              <a href="#" class="c-teaser__link">Read More</a>
            </div>
          </div>
        </div>

</div>
</div>

<div class="o-grid">
  <div class="lg-12">
    <?php print render($page['content_pre']); print render($page['content']); print render($page['content_post']); ?>

    <?php /** Output the field_widgets html. **/ ?>
    <?php if (isset($page['display_field_widgets'])): ?>
      <?php print render($page['display_field_widgets']); ?>
    <?php endif; ?>

  </div>
</div>

<?php include 'common/footer.inc'; ?>
