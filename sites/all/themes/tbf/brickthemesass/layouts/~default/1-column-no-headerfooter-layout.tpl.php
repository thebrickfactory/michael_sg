<?php
/**
 * One column layout.
 */
?>

<div class="o-grid">
  <div class="lg-12">

    <?php include 'common/messages.inc'; ?>
    <?php include 'common/title.inc'; ?>

    <?php print render($page['content_pre']); print render($page['content']); print render($page['content_post']); ?>

    <?php /** Output the field_widgets html. **/ ?>
    <?php if (isset($page['display_field_widgets'])): ?>
      <?php print render($page['display_field_widgets']); ?>
    <?php endif; ?>

  </div>
</div>
