<?php
/**
 * Two column (right) layout. The sidebar column is to the right of the content.
 */
?>

<?php include 'common/header.inc'; ?>

<div class="o-grid">
  <div class="lg-12">

    <?php include 'common/breadcrumbs.inc'; ?>
    <?php include 'common/messages.inc'; ?>

    <div class="o-grid">
      <section class="lg-8">
        <?php include 'common/title.inc'; ?>

        <?php print render($page['content_pre']); print render($page['content']); print render($page['content_post']); ?>

        <?php /** Output the field_widgets html. **/ ?>
        <?php if (isset($page['display_field_widgets'])): ?>
          <?php print render($page['display_field_widgets']); ?>
        <?php endif; ?>

      </section>

      <aside class="lg-4">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    </div>
  </div>
</div>

<?php include 'common/footer.inc'; ?>
