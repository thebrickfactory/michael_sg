<?php
/***
 * Custom Home Page layout.
 **/
?>

<?php
/** PHP variables that are used to manually display boxes and blocks.
 *
 * [Programming Block]
 * $problem_solvers = module_invoke('tbf_grid_thumbs', 'block_view', 'meet_problem_solvers_home');
 * <div class="row lightgray-bg">
 *   <div class="large-12 columns"><h2 class="title text-center"><?php echo isset($problem_solvers['subject']) ? $problem_solvers['subject'] : ''; ?></h2><?php print render($problem_solvers); ?></div>
 * </div>
 *
 * [Box]
 * $under_col1 = tbf_blocks_get_box('home_underslider_1');
 * <?php print render($under_col1); ?>
 *
 * [Webform]
 * $problem_solvers_daily_form = module_invoke('webform', 'block_view', 'client-block-1775');
 * <?php print render($problem_solvers_daily_form['content']); ?>
 *
*/
?>

<?php include 'common/header.inc'; ?>

<?php include 'common/breadcrumbs.inc'; ?>
<?php include 'common/messages.inc'; ?>

<div class="o-grid">
  <div class="lg-12">
    [CUSTOM HOME PAGE LAYOUT]

    <?php print render($page['content_pre']); print render($page['content']); print render($page['content_post']); ?>

    <?php /** Output the field_widgets html. **/ ?>
    <?php if (isset($page['display_field_widgets'])): ?>
      <?php print render($page['display_field_widgets']); ?>
    <?php endif; ?>

  </div>
</div>

<?php include 'common/footer.inc'; ?>
