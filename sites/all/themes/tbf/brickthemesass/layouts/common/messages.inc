<?php
/**
 * Display sitewide messages.
 *
 * To change the html for the messages, see bricktheme_status_messages() in
 * php/core/theme.inc.php.
 */
?>

<?php if($messages): ?>
  <div class="o-grid">
    <div class="l-12">
    	<?php print $messages; ?>
    </div>
  </div>
<?php endif; ?>