<?php
/**
 * Display sitewide breadcrumbs.
 *
 * The way the breadcrumbs look, including the divider, is controlled by the
 * css in stylesheets/foundation.css. If you actually need to modify the html
 * for the breadcrumbs, see bricktheme_breadcrumb() in php/core/theme.inc.php.
 */
?>
<?php if($breadcrumb): ?>
<div class="o-grid">
  <div class="lg-12">
  	<ul class="c-crumbs">
  		<?php print $breadcrumb; ?>
  	</ul>
  </div>
</div>
<?php endif; ?>