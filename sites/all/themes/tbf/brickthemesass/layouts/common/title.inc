<?php
/**
 * Display the page title, action links and tabs (for logged in users).
 */
?>

<?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?><?php print render($tabs); ?><?php endif; ?>

<?php if (!empty($title)): ?>
  <?php print render($title_prefix); ?><h1 class="text-center"><?php print $title; ?></h1><?php print render($title_suffix); ?>
<?php endif; ?>

<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
