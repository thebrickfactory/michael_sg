<?php
/*
 * The default header for the site.
 */
?>

<header class="c-page-head">
  <div class="o-grid">
    <div class="lg-3"><a href="/" class="c-logo--default"></a></div>
    <div class="lg-7">

      <?php
      /* MAIN MENU */
      if($main_menu): ?>
          <div class="o-grid">
            <div class="lg-12">
              <?php print render($main_menu); ?>
            </div>
          </div>
      <?php endif; ?>

    </div>
    <div class="lg-2 c-sharebar">
      <a href="" title="link-1"></a>
      <a href="" title="link-2"></a>
      <a href="" title="link-3"></a>

    </div>
  </div>
</header>

<a href="#" class="c-mobile-toggle">Menu</a>