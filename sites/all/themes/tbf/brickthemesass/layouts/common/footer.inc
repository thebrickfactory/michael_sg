<?php
/*
 * The default footer for the site.
 */
?>

<footer class="o-band c-page-foot">
  <div class="o-grid">
      <div class="lg-12 t-flex-center">
        <?php print render($main_menu); ?>
      </div>
    <?php /*
    <div class="md-2 sm-12">
      <?php
          // what the f#$ck is this?
          // no way for someone to know what this is or where it's set
          // we need to have some php comments at top of layout files
      ?>
      <?php print isset($footer_menu_1) ? $footer_menu_1 : ''; ?>
    </div>
    <div class="md-2 sm-12">  
      <?php print isset($footer_menu_2) ? $footer_menu_2 : ''; ?>
    </div>
    <div class="md-2 sm-12"> 
      <?php print isset($footer_menu_3) ? $footer_menu_3 : ''; ?>
    </div>
    <div class="md-2 sm-12">  
      <?php print isset($footer_menu_4) ? $footer_menu_4 : ''; ?>
    </div>  
    <div class="md-2 sm-12">
      <?php print isset($footer_menu_5) ? $footer_menu_5 : ''; ?>
    </div>
    <div class="md-2 sm-12">  
      <?php print $footer_search; ?>
    </div>
    */ ?>
  </div>
  <a href="/" class="c-logo--default t-center"></a>
</footer>

<?php
/**
 * We need the reveal divs to appear outside the layout so it appears right.
 */
if ($page['reveal']) {
  print render($page['reveal']);
}
?>

