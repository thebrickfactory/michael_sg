
[SETUP]
If you are setting up a new drupal project for the first time, go to this url
on the docs site: /documentation/developer_process/drupal7_site_setup


[LAYOUTS]
This theme supports multiple layouts. This is what you need to do in order
to enable a new layout:

1. Add the new layout, with the correct regions, in the layouts section of the
bricktheme.info file. Make sure to point it to the correct layout file.

2. Add/update the layout in the layouts folder. We created some default layouts
in the layouts/~default directory. Feel free to copy those.

3. Trigger the layout to display on a page, including setting a default layout,
through the context module.

Under the layouts/~common directory, there are a few layout related files
that we re-use in the layout files.


[IMAGES]
All images that are required by the theme (like logo, bg images, etc...)
should go into the img folder


[CSS]
We use Sass to generate the css. The gulp file is meant to be run from the
docroot of the site. Make sure to copy these files to the docroot: gulpfile.js,
config.json and package.json . Run sudo npm install. And then gulp from the
docroot. This will generate the includes CSS and JS file.


[JAVASCRIPT]
The gneral js file that is loaded on all pages of the site is in js/footer/app.js
