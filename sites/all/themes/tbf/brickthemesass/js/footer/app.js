

jQuery(function($){
	// Used for things like the smoothscroll where a fixed nav is used.
	var fixed_nav_height = 258;

	/** Smoothscroll **/	
  $('.smoothscroll').click(function(e){
    e.preventDefault();
    var offset = fixed_nav_height;
    if ($(window).width() < 640) offset = 0;
    $('html,body').animate({scrollTop:$(this.hash).offset().top - offset}, 1000);
  });

	$('.c-primary-nav').tbfMobileMenu();

  // Forms
  if( $('.c-form')[0] ) {
    $('.c-form').parsley({
      errorsWrapper: '<ul class="c-form__field_errors"></ul>'
    });
  }

});


