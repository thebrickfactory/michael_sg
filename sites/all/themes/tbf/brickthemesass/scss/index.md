## Welcome

This is the default style-guide used for all projects at TBF.  Please be sure you reference this document before making any changes on a project.  We also require that you extensively document your code using the hologram doc commenting system like below:

```
/*doc
---
title: Buttons
name: buttons
category: Components - Buttons
---

Description goes here

*/
```

Please visit our [doc site](http://docs.thebrickfactory.com/documentation/developer_process/css_guidelines/start) to view all CSS guidelines.  Click [here](/styleguide) to jump to the example file created with these components.