<?php

/**
 * Implements theme_checkbox().
 */
function brickthemesass_checkbox($variables) {
  $element = $variables['element'];
  $t = get_t();
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array(
      'id',
      'name',
      '#return_value' => 'value',
    )
  );

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form__check'));

  switch ($element['#title_display']) {
    case 'attribute':
      $element['#attributes']['#title'] = $element['#description'];
      $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
      break;

    // Bootstrap's default styles cause 'before' to render exactly like 'after':
    case 'before':
      $output = '<div class="c-form__sub_item"><label class="c-form__inlabel" for="' . $element['#attributes']['id'] . '">' . $element['#description'] . '</label><input' . drupal_attributes() . ' /></div>';
      break;

    // The 'invisible' option really makes no sense in the context of checkboxes
    // or radios, so just treat it like 'after':
    case 'invisible':
    case 'after':
    default:
      // There are some odd cases, such as the module list page, where
      // checkboxes apparently themed with a #title_display value of 'before'
      // when it should really be 'attribute'. The two possible outcomes handle
      // this problem:
      $checkbox = '<input' . drupal_attributes($element['#attributes']) . ' />';
      if (isset($element['#description'])) {
        $output = '<div class="c-form__sub_item">' . $checkbox . ' <label class="c-form__inlabel" for="' . $element['#attributes']['id'] . '">' . $element['#description'] . '</label></div>';
      }
      else {
        $output = $checkbox;
      }
      break;

  }

  return $output;
}


/**
 * Implements theme_checkboxes().
 */
function brickthemesass_checkboxes($variables) {
  // Redefine #children:
  $option_children = '';
  foreach ($variables['element']['#options'] as $key => $description) {
    $option_variables = array(
      'element' => $variables['element'][$key],
    );
    $option_variables['element']['#description'] = $option_variables['element']['#title'];
    $option_children .= theme('checkbox', $option_variables) . "\n";
  }
  $variables['element']['#children'] = $option_children;
  // Proceed normally:
  $element = $variables['element'];
  // Attributes usually figured out here, but serve no purpose here (?)
  return !empty($element['#children']) ? $element['#children'] : '';
}

/**
 * Implements theme_radio().
 */
function brickthemesass_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array(
      'id',
      'name',
      '#return_value' => 'value',
    )
  );

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('c-form__radio'));

  switch ($element['#title_display']) {
    // This *could* make sense e.g. in a table of mutually exclusive options...
    case 'attribute':
      $element['#attributes']['#title'] = $element['#description'];
      $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
      break;

    // Bootstrap's default styles cause 'before' to render exactly like 'after':
    case 'before':
      $output = '<div class="c-form__sub_item"><label class="c-form__inlabel" for="' . $element['#attributes']['id'] . '">' . $element['#description'] . '</label> <input' . drupal_attributes($element['#attributes']) . ' /></div>';
      break;

    // The 'invisible' option really makes no sense in the context of checkboxes
    // or radios, so just treat it like 'after':
    case 'invisible':
    case 'after':
    default:
      $radio = '<input' . drupal_attributes($element['#attributes']) . ' />';
      if (isset($element['#description'])) {
        $output = '<div class="c-form__sub_item">' . $radio . ' <label class="c-form__inlabel" for="' . $element['#attributes']['id'] . '">' . $element['#description'] . '</div>';
      }
      else {
        $output = $radio;
      }
      break;

  }

  return $output;
}


/**
 * Implements theme_radios().
 */
function brickthemesass_radios($variables) {
  // Redefine #children:
  $option_children = '';
  foreach ($variables['element']['#options'] as $key => $description) {
    $option_variables = array(
      'element' => $variables['element'][$key],
    );
    $option_variables['element']['#description'] = $option_variables['element']['#title'];
    $option_children .= theme('radio', $option_variables) . "\n";
  }
  $variables['element']['#children'] = $option_children;
  $element = $variables['element'];
  // Attributes usually figured out here, but serve no purpose here (?)
  return !empty($element['#children']) ? $element['#children'] : '';
}

/**
 * Implements theme_select().
 */
function brickthemesass_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));

  if (!is_array($element['#attributes']['class'])) {
    $element['#attributes']['class'] = array();
  }

  array_unshift($element['#attributes']['class'], 'c-form__email', 'c-form__input');

  return '<select' . drupal_attributes($element['#attributes']) . '>' . brickthemesass_form_select_options($element) . '</select>';
}

/**
 * Custom implementation of form_select_options().
 */
function brickthemesass_form_select_options($element, $choices = NULL) {
  if (!isset($choices)) {
    $choices = $element['#options'];
  }
  // array_key_exists() accommodates the rare event where $element['#value'] is NULL.
  // isset() fails in this situation.
  $value_valid = isset($element['#value']) || array_key_exists('#value', $element);
  $value_is_array = $value_valid && is_array($element['#value']);
  $options = '';
  foreach ($choices as $key => $choice) {
    if (is_array($choice)) {
      $options .= '<optgroup label="' . check_plain($key) . '">';
      $options .= brickthemesass_form_select_options($element, $choice);
      $options .= '</optgroup>';
    }
    elseif (is_object($choice)) {
      $options .= brickthemesass_form_select_options($element, $choice->option);
    }
    else {
      $key = (string) $key;
      if ($value_valid && (!$value_is_array && (string) $element['#value'] === $key || ($value_is_array && in_array($key, $element['#value'])))) {
        $selected = ' selected="selected"';
      }
      else {
        $selected = '';
      }
      $options .= '<option value="' . check_plain($key) . '" class="c-form__option"' . $selected . '>' . check_plain($choice) . '</option>';
    }
  }
  return $options;
}
