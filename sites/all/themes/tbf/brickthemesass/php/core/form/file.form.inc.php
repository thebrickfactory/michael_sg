<?php

/**
 * Implements theme_file().
 */
function brickthemesass_file($variables) {
  $variables['element']['#attributes']['size'] = '22';
  $element = $variables['element'];
  $element['#attributes']['type'] = 'file';
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('c-form__file'));
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Implements theme_webform_managed_file().
 */
function brickthemesass_webform_managed_file($variables) {
  $element = $variables['element'];

  $attributes = array();

  // For webform use, do not add the id to the wrapper.
  //if (isset($element['#id'])) {
  //  $attributes['id'] = $element['#id'];
  //}

  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'c-form__managed_file';

  // This wrapper is required to apply JS behaviors and CSS styling.
  $output = '';
  $output .= '<div' . drupal_attributes($attributes) . '>';
  $output .= drupal_render_children($element);
  $output .= '</div>';
  return $output;
}