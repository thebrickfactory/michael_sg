<?php

/**
 * Implements theme_textarea().
 */
function brickthemesass_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));

  if (!isset($element['#attributes']['class']) || !is_array($element['#attributes']['class'])) {
    $element['#attributes']['class'] = array();
  }

  array_unshift($element['#attributes']['class'], 'c-form__textarea');

  $wrapper_attributes = array('class' => array('c-form__sub_item'));

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}

/**
 * Implements theme_textfield().
 */
function brickthemesass_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
    )
  );

  if (!is_array($element['#attributes']['class'])) {
    $element['#attributes']['class'] = array();
  }

  array_unshift($element['#attributes']['class'], 'c-form__text', 'c-form__input');

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}

/**
 * Implements theme_email().
 */
function brickthemesass_webform_email($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'email';
  element_set_attributes($element, array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
    )
  );

  if (!is_array($element['#attributes']['class'])) {
    $element['#attributes']['class'] = array();
  }

  array_unshift($element['#attributes']['class'], 'c-form__email', 'c-form__input');

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output  ;
}

/**
 * Implements theme_password().
 */
function brickthemesass_password($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element, array('id', 'name', 'size', 'maxlength'));
  _form_set_class($element, array('form-text', 'input-text'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
