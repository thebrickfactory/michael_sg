<?php
require_once 'pager.inc.php';
require_once 'form.inc.php';
require_once 'menus.inc.php';

/*
 * Default theme updates that usually won't need to be updated from site to
 * site.
 */

/**
 * Implements hook_html_head_alter().
 */
function brickthemesass_html_head_alter(&$head_elements) {
  // Delete some default Mothership stuff.
  unset($head_elements['system_meta_generator'], $head_elements['my_meta']);
}

/**
 * Implements theme_status_messages(). Update the error message html to use
 * the Foundation format.
 */
function brickthemesass_status_messages($variables) {
  $display = $variables['display'];

  // Map the error type to the correct foundation class.
  $status_map = array(
    'status' => 'success',
    'error' => 'alert',
    'warning' => 'secondary',
  );

  $output = '';
  foreach (drupal_get_messages($display) as $type => $messages) {
    $class = $status_map[$type];
    foreach ($messages as $message) {
      $output .= _brickthemesass_status_messages($class, $message);
    }
  }

  return $output;
}

/*
 * Generate the html for the messages.
 */
function _brickthemesass_status_messages($class, $message) {
  $output = "<div data-alert class=\"alert-box $class\">";
  $output .= $message;
  $output .= '<a href="" class="close">&times;</a>';
  $output .= "</div>";
  return $output;
}

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
function brickthemesass_breadcrumb($variables) {
  if (!empty($variables['breadcrumb'])) {
    $variables['breadcrumb'][] = array(
      'class' => array('current'),
      'data' => drupal_get_title(),
    );
    $breadcrumb = $variables['breadcrumb'];

    // For screen readers.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= theme('item_list', array(
        'items' => $breadcrumb,
        'type' => 'ul',
        'attributes' => array('class' => array('breadcrumbs')),
      )
    );

    return $output;
  }
}

/**
 * Implements hook_js_alter().
 *
 * Deletes all JS and only adds the files we want. Adds the foundation JS.
 */
function brickthemesass_js_alter(&$js) {
  // Delete all JS files in sites/all/modules and misc/.
  // We do not want to delete JS files included from custom modules!
  $exceptions = array('sites/all/modules/jcaption/jcaption.js', );
  foreach ($js as $file => $value) {
    if (in_array($file, $exceptions)) {
      continue;
    }

    if (substr($file, 0, 8) == 'modules/' || substr($file, 0, 18) == 'sites/all/modules/' || strpos($file, 'misc/') !== FALSE) {
      unset($js[$file]);
    }
  }

  $theme_path = drupal_get_path('theme', variable_get('theme_default', NULL));

  // Add the header script file.
  $header_script = $theme_path . '/processed/js/app_header.min.js';
  $js[$header_script] = array(
    'group' => -100,
    'weight' => -19.995,
    'every_page' => TRUE,
    'type' => 'file',
    'scope' => 'header',
    'cache' => TRUE,
    'preprocess' => TRUE,
    'data' => $header_script,
    'defer' => '',
  );

  // Add the footer script file.
  $footer_script = $theme_path . '/processed/js/app_footer.min.js';
  $js[$footer_script] = array(
    'group' => -100,
    'weight' => -19.996,
    'every_page' => TRUE,
    'type' => 'file',
    'scope' => 'footer',
    'cache' => TRUE,
    'preprocess' => TRUE,
    'data' => $footer_script,
    'defer' => '',
  );
}

/**
 * Implements hook_css_alter().
 *
 * Deleted all CSS that mothership missed. Add the foundation css.
 */
function brickthemesass_css_alter(&$css) {
  
  // Delete all CSS files in sites/all/modules and misc/.
  // We do not want to delete JS files included from custom modules!
  $exceptions = array();
  foreach ($css as $file => $value) {
    if (in_array($file, $exceptions)) {
      continue;
    }

    if (substr($file, 0, 8) == 'modules/' || substr($file, 0, 18) == 'sites/all/modules/' || strpos($file, 'misc/') !== FALSE) {
      unset($css[$file]);
    }
  }

  $theme_path = drupal_get_path('theme', variable_get('theme_default', NULL));

  // Global styles for this site.
  $global = $theme_path . '/processed/css/app.css';
  $css[$global] = array(
    'group' => -100,
    'every_page' => TRUE,
    'media' => 'all',
    'type' => 'file',
    'weight' => 0.003,
    'preprocess' => TRUE,
    'data' => $global,
    'browsers' => array('IE' => TRUE, '!IE' => TRUE),
  );
}

/**
 * Implements hook_page_alter(). Delete the admin toolbar from the front end theme.
 */
function brickthemesass_page_alter(&$page) {
  unset($page['page_bottom']['admin_toolbar'], $page['page_top']['toolbar']);
}

function brickthemesass_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link['localized_options']['html'] = TRUE;
  $link['localized_options']['attributes']['class'] = array();
  $link['localized_options']['attributes']['class'][] = 'c-btn';
  $link['localized_options']['attributes']['class'][] = '';
  return '<li>' . l($link['title'], $link['href'], $link['localized_options']) . '</li>'."\n";
}

function brickthemesass_menu_local_tasks($variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<ul class="o-nav g-drupal-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

function brickthemesass_form_alter(&$form, &$form_state, $form_id) {
  if($form_id == 'webform_client_form_1775') { // Ex: "webform_client_form_33"
    foreach ($form["submitted"] as $key => $value) {
      if (in_array($value["#type"], array("textfield", "webform_email", "textarea"))) {
        $form["submitted"][$key]['#attributes']["placeholder"] = t($value["#title"]);
      }
    }
  }
}