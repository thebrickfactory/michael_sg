<?php

/**
 * Customize the main menu.
 */
function brickthemesass_menu_tree__main_menu($variables){
  return '<ul class="c-primary-nav">' . $variables['tree'] . '</ul>';
}

function brickthemesass_menu_link__main_menu($variables) {
  $element = $variables['element'];

  if (in_array("active-trail", $element["#attributes"]["class"]) || ($element['#title'] == 'Home' && drupal_is_front_page())) {
    $element['#attributes']['class'][] = 'c-primary-nav__item active';
    
  }
  else {
    $element['#attributes']['class'][] = 'c-primary-nav__item' ;
  }

  $sub_menu = '';

  if ($element['#below']) {
    // echo '<pre>'; print_r($element['#below']); echo '</pre>'; exit;
    $element['#attributes']['class'][0] = 'has-dropdown';
    $sub_menu = drupal_render($element['#below']);
    $sub_menu = str_replace(array('<ul class="c-primary-nav">'), array('<ul class="c-drop-nav o-nav o-nav--stacked">'), $sub_menu);
    $sub_menu = str_replace(array('c-primary-nav__item', 'c-primary-nav__link'), array('c-drop-nav__item', 'c-drop-nav__link'), $sub_menu);

  }

  $element['#localized_options']['attributes']['class'][] = 'c-primary-nav__link';
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  if (!empty($sub_menu)) {
    $sub_menu = "<div class='c-drop-nav__wrap'>" . $sub_menu . "</div>";
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Submenu.
 */
function brickthemesass_menu_tree__menu_block__1($variables){
  return '<dl class="sub-nav">' . $variables['tree'] . '</dl>';
}

function brickthemesass_menu_link__menu_block__1($variables) {
  $element = $variables['element'];
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<dd ' . drupal_attributes($element['#attributes']) . '>' . $output . "</dd>\n";
}