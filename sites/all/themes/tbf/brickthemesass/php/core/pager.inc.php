<?php
/**
 * Returns HTML for a query pager.
 *
 * Menu callbacks that display paged query results should call theme('pager') to
 * retrieve a pager control so that users can view other results. Format a list
 * of nearby pages with additional query results.
 *
 * @param $variables
 *   An associative array containing:
 *   - tags: An array of labels for the controls in the pager.
 *   - element: An optional integer to distinguish between multiple pagers on
 *     one page.
 *   - parameters: An associative array of query string parameters to append to
 *     the pager links.
 *   - quantity: The number of pages in the list.
 *
 * @ingroup themeable
 */
function brickthemesass_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Re-calculate qty in order not to print the whole pager when
  // result pages are less than 9
  if ($pager_total[0] < $quantity) {
    $quantity = $pager_total[0];
  }
  
  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  //$li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  //$li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));


  if ($pager_total[$element] > 1) {

    /*
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    */

    if ($li_previous) {
      $items[] = array(
        'class' => array('c-pager__item', 'c-pager__previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {

      /*
      if ($i > 1) {
        $items[] = array(
          'class' => array('unavailable'),
          'data' => '&hellip;',
        );
      }
      */

      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('c-pager__item'),
            'data' => l($i, current_path(), array(
              'attributes' => array(
                'class' => array('c-pager__link'),
              ),
              'query' => array('page' => ($pager_current - $i)),
            )),
          );
        }

        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('c-pager__item', 'c-pager__selected', 'unavailable'),
            'data' => l($i, current_path(), array(
              'attributes' => array(
                'class' => array('c-pager__link'),
              ),
              'query' => array('page' => ($i - 1)),
            )),
          );
        }

        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('c-pager__item'),
            'data' => l($i, current_path(), array(
              'attributes' => array(
                'class' => array('c-pager__link'),
              ),
              'query' => array('page' => ($i - $pager_current)),
            )),
          );
        }
      }

      /*
      if ($i <= $pager_max) {
        $items[] = array(
          'class' => array('unavailable'),
          'data' => '&hellip;',
        );
      }
      */

    }
    // End generation.

    if ($li_next) {
      $items[] = array(
        'class' => array('c-pager__item', 'c-pager__next'),
        'data' => $li_next,
      );
    }

    /*
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    */

    $html = '';
    $html .= '<div class="c-pager__wrapper">';
    $html .= theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('c-pager')),
    ));
    $html .= '</div>';

    return $html;
  }
}

/**
 * Returns HTML for a link to a specific query result page.
 *
 * @param $variables
 *   An associative array containing:
 *   - text: The link text. Also used to figure out the title attribute of the
 *     link, if it is not provided in $variables['attributes']['title']; in
 *     this case, $variables['text'] must be one of the standard pager link
 *     text strings that would be generated by the pager theme functions, such
 *     as a number or t('« first').
 *   - page_new: The first result to display on the linked page.
 *   - element: An optional integer to distinguish between multiple pagers on
 *     one page.
 *   - parameters: An associative array of query string parameters to append to
 *     the pager link.
 *   - attributes: An associative array of HTML attributes to apply to the
 *     pager link.
 *
 * @see theme_pager()
 *
 * @ingroup themeable
 */
function brickthemesass_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574
  $attributes['href'] = url($_GET['q'], array('query' => $query));

  // Pagination with rel=“next” and rel=“prev”. Does not support well multiple
  // pagers on the same page - it will create relnext and relprev links
  // in header for that case only for the first pager that is rendered.
  static $rel_prev = FALSE, $rel_next = FALSE;
  if (!$rel_prev && $text == t('‹ previous')) {
    $rel_prev = TRUE;
    drupal_add_html_head_link(array('rel' => 'prev', 'href' => url($_GET['q'], array('query' => $query, 'absolute' => true))));
  }
  if (!$rel_next && $text == t('next ›')) {
    $rel_next = TRUE;
    drupal_add_html_head_link(array('rel' => 'next', 'href' => url($_GET['q'], array('query' => $query, 'absolute' => true))));
  }

  return '<a' . drupal_attributes($attributes) . '>' . check_plain($text) . '</a>';
}