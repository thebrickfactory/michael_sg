<?php
/*
Put all preprocess functions in here. Some examples:

TEMPLATE_preprocess_html()
TEMPLATE_preprocess_page()
TEMPLATE_preprocess_node()
TEMPLATE_preprocess_comment()
TEMPLATE_preprocess_region()
*/

/**
 * template_preprocess_page().
 */
function brickthemesass_preprocess_page(&$variables) {
  // Print out the full structure for the main menu.
  $main_menu_data = menu_tree_page_data('main-menu');
  $main_menu = menu_tree_output($main_menu_data);
  $variables['main_menu'] = $main_menu;

  $footer_menu_data = menu_tree_page_data('menu-footer-menu');
  $footer_menu_final = menu_tree_output($footer_menu_data);

  // For the footer menu, we are grabbing pieces of the main menu and defining
  // PHP vars to use in the footer template.
  
  $count = 1;
  foreach ($footer_menu_data as $menu_key => $menu_item) {
    if ($count > 5) {
      break;
    }

    $var_name = 'footer_menu_' . $count;
    $footer_menu = array($footer_menu_data[$menu_key]);
    $footer_menu_html = menu_tree_output($footer_menu);
    $footer_menu_html = render($footer_menu_html);

    // Take out the html that is making this use the Foundation menu.
    $variables[$var_name] = str_replace(array(' class="nav-bar"', ' class="flyout"', 'has-flyout'), '', $footer_menu_html);
    $count++;
  }
  

  // Add a variable for the footer search.
  //$search_form_html = drupal_get_form('search_block_form');
  //$search_form_html = drupal_render($search_form);
  //$search_form_html = str_replace('form-submit button radius', '', $search_from_html);
  $search_form_html = '';
  $variables['footer_search'] = $search_form_html;

  // Add the widgets
  if (isset($variables['node'])) {
    bricktheme_widgets($variables, $variables['node']);
  }

}

/**
 * Sets the page varaibles for the widget regions.
 *
 * If we are on national, it uses the widget fields themselves. If this is a state
 * site, we grab the data from the cache.
 */
function bricktheme_widgets(&$variables, $node) {
  if (!class_exists('DBWidget_Config') || !function_exists('tbf_data_modules_output')) {
    return;
  }

  $widget_fields = DBWidget_Config::$widget_field_names;
  foreach ($widget_fields as $field_name) {
    if (isset($node->{$field_name}['und'])) {
      $var_name = 'display_' . $field_name;
      $variables['page'][$var_name][]['#markup'] = tbf_data_modules_output($node, $field_name);
    }
  }
}

/**
 * Implements hook_ds_pre_render_alter().
 */
function brickthemesass_ds_pre_render_alter(&$layout_render_array, $context) {
  if (isset($layout_render_array['main_nowrapper'])) {
    $orig_title = $layout_render_array['main_nowrapper'][0]['#object']->title;
    $new_title = _brickthemesass_shorten_title($orig_title);
    if ($orig_title != $new_title) {
      $layout_render_array['main_nowrapper'][2][0]['#markup'] = str_replace($orig_title, $new_title, $layout_render_array['main_nowrapper'][2][0]['#markup']);
    }
  }
}

/**
 * Implements hook_views_pre_render();
 */
function brickthemesass_views_pre_render(&$view) {
  if($view->name == "the_latest" && $view->current_display == 'block_1') {
    foreach ($view->result as &$result) {
      $result->node_title = _brickthemesass_shorten_title($result->node_title, 50);
    }
    //echo '<pre>'; print_r($view); echo '</pre>'; exit;
  }
}

/**
 * Shorten a string for the title.
 */
function _brickthemesass_shorten_title($title, $char_length=60) {
  $your_desired_width = $char_length;
  if (strlen($title) > $your_desired_width) {
    $title = preg_replace('/\s+?(\S+)?$/', '', substr($title, 0, $your_desired_width)) . '...';
  }
  return $title;
}