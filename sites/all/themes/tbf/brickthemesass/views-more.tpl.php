<?php

/**
 * @file
 * Theme the more link.
 * Added Foundation Classes 031913 
 *
 * - $view: The view object.
 * - $more_url: the url for the more link.
 * - $link_text: the text for the more link.
 *
 * @ingroup views_templates
 */
?>

<div class="more-link right">
  <a href="<?php print $more_url ?>" class="button">
    <?php print $link_text; ?>
  </a>
</div>
