<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php print $head_title; ?></title>

  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<?php print $page_top; ?>

<?php /** FB + Twitter **/ ?>
<?php
/*
<div id="fb-root"></div>
<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=192445220910331";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
*/
?>

<?php global $user; if ((is_array($user->roles) && in_array('administrator', $user->roles)) || $user->uid == 1): ?>
  <div class="admin-bar">
    <div class="o-grid">
      <div class="lg-12 t-text-right">
        <strong>Admin Shortcuts:</strong>
        <a href="/admin/dashboard/">Dashboard</a> |
        <a href="/node/add/">Add Content</a> |
        <a href="/admin/content/">Find Content</a> |
        <a href="/admin/config/">Configuration</a> |
        <a href="/<?php print path_to_theme(); ?>/styleguide/docs/index.html">Style Guide</a> |
        <a href="/user/logout/">Logout</a>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="o-wrap">
  <?php print $page; ?>
</div>

<?php print $page_bottom; ?>

</body>
</html>