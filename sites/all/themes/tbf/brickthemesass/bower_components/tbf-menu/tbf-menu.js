/**
 * Offcanvas menu plugin
 * -----------------------------------
 * 
 */
(function ( $ ) {
  $.fn.tbfMobileMenu = function(options) {
    var settings = $.extend({
      overlay_class: "c-overlay", // Site overlay component 
      nav_class: "c-primary-nav", // This should really be whatver the plugin is run on, but I don't know how to do that...
      wrap_class: "o-wrap",  // Site wrapper div
      trigger_class: "c-mobile-toggle" // Toggle button
    }, options);

    // Shifts the body and nav when the mobile trigger link is clicked
    $('.' + settings.trigger_class).on('click', function() {
      $(this).closest('.' + settings.wrap_class).toggleClass('is-shifted');
      $('.' + settings.nav_class).toggleClass('is-active');
      $('html').toggleClass('is-active');
      $('.' + settings.overlay_class).toggleClass('is-active');
    });

    // Shifts the menu back when the site overlay is clicked
    $('.' + settings.overlay_class).on('click', function() {
      $('.' + settings.wrap_class).toggleClass('is-shifted');
      $('.' + settings.nav_class).toggleClass('is-active');
      $('html').toggleClass('is-active');
      $('.' + settings.overlay_class).toggleClass('is-active');
    });

    // Accordion functionality for menu items.
    $('body').on('click', '.is-shifted .c-primary-nav__link', function(e) {
      e.preventDefault();
      var subnav = $(this).next();

      // Collapses accordion if already active
      if( $(this).hasClass('is-up') ) {
        $(this).next().removeClass('is-expanded');
        $(this).removeClass('is-up');
      }  

      // Removes all expands and expands current  
      else {
        $('.c-drop-nav__wrap.is-expanded').removeClass('is-expanded');
        $('.c-primary-nav__link').removeClass('is-up');
        $(this).addClass('is-up');
        $(this).next().addClass('is-expanded');
      }    
    });
  }
}( jQuery ));