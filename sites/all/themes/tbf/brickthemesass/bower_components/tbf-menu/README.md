# TBF Menu
Repo contains JS for the default TBF menu and an HTML example

## HTML
In order to make this menu work you'll need to follow a fairly strict HTML structure. 
Each piece of HTML you need is outlined below.


### Page wrapper
In order to slide the page over (off-canvas) we'll need an outer wrapper.  Take a look at the example HTML below:

```
<div class="o-wrap">
	[everything goes here]
</div>
```
In this case we are using the `o-wrap` class which is our wrap object.

### Nav HTML
Below is the example HTML for the primary-nav component.

```
<ul class="c-primary-nav">

	<!-- Nav item -->
	<li class="c-primary-nav__item">
		<a class="c-primary-nav__link" href="#">Home</a>
	</li>

	<!-- Nav item (with drop-down-->
	<li class="c-primary-nav__item has-dropdown">
		<a class="c-primary-nav__link" href="#">About us</a>
		<div class="c-drop-nav__wrap">
			<ul class="c-drop-nav o-nav o-nav--stacked">
				<li class="c-drop-nav__item"><a href="#" class="c-drop-nav__link">Example link</a></li>
				<li class="c-drop-nav__item"><a href="#" class="c-drop-nav__link">Another link</a></li>
				<li class="c-drop-nav__item"><a href="#" class="c-drop-nav__link">Simple link</a></li>
				<li class="c-drop-nav__item"><a href="#" class="c-drop-nav__link">Complex link</a></li>
				<li class="c-drop-nav__item"><a href="#" class="c-drop-nav__link">External link</a></li>
			</ul>	
		</div>
	</li>
</ul>
```